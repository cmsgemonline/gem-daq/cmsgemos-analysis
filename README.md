# GEM Online Analysis

If you are a developer please start with reading the [Contributor's ][contributors] and [Developers ][setup] Guides

To be updated...

## Getting stated
Before starting to use this GEM DAQ analysis package you need to install it and its dependences, to that end, `pyproject.toml` file is provided here and you must execute the command 

``` shell
git clone https://gitlab.cern.ch/cmsgemonline/gem-daq/cmsgemos-analysis.git
cd cmsgemos-analysis/
poetry install
```
Poetry is a dependency manager for python which produces an appropiate virtual enviroment to run the different analyses. For futher information about poetry usage you can visit the official [documentation][poetryDoc].

If you are working in a GEM development machine, you can be guided instead by the [quick start][quickStart] of the Developers Guide. 

## Running analysis
The cmsgemos-analysis package has a set of different routines to analyze the output of GEM DAQ calibration scans and to produce needed configuration files for the detector.

Aiming to perform some analysis, you can use the command `gemos` with its suitable arguments, which you can explore with `gemos --help` or in its equivalent short way `gemos -h`. 

Bear in mind that all dependencies live inside a poetry virtual environment, then, the prefix `poetry run` should be added to all commands. Otherwise, the right environment can be achieved by using the `poetry shell`.

Explore the available analyses and enjoy!

[contributors]:CONTRIBUTING.md
[setup]:DEVELOPERS.md#setup
[poetryDOC]:https://github.com/python-poetry/poetry
[quickStart]:DEVELOPERS.md#quick-start

