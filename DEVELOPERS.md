# Developers' guide

[[_TOC_]]

## Development setup

The recommended way of development is under a virtual environment managed through `poetry` on an official GEM development machine.
Similar instructions are applicable to non-virtual environments and personal machines too, but it is neither advised nor supported.
Use at your own risk.

### Requirements

First of all, you need to have `python` version `>=3.8.6, <3.12` available on your development machine.
Ensuring this is your own responsibility, as different operation systems are supplied with different Python interpreters and different methods are used to install modern Python interpreter alongside with the system one.
On CERN's CC7 `lxplus` machines the recommended way is to [source an appropriate software collection](https://cern.service-now.com/service-portal?id=kb_article&n=KB0000730).
On official GEM development machines `python3.8` (CC7) or `python3.9` (Alma 9) is already installed.

In addition, a `PostgreSQL` server version `10` must be running.
On official GEM development machines this is also taken care of by your sysadmins, every user is provided a personal database.

### Quick start

0. Before starting read this guide further to get familiar with our development practices and tools
1. Set up your environment (to be done every time you start a new session).
   On official CC7 GEM development machines, this can be resumed with the following commands:
   ``` shell
   source scl_source enable rh-git218 # Newer Git release (optional)
   source scl_source enable rh-python38 # Python 3.8 release (optional)
   export PATH=/opt/poetry/bin:$PATH # Poetry
   ```
   On official Alma 9 GEM development machines, this can be resumed with the following commands:
   ``` shell
   export PATH=/opt/poetry/bin:$PATH # Poetry
   ```
2. Clone the repository `git clone https://gitlab.cern.ch/cmsgemonline/gem-daq/cmsgemos-analysis.git && cd cmsgemos-analysis`
3. Install the `cmsgemos-analysis` project along with all its dependencies in a virtual environment with the command `poetry install`
4. You are ready... if you have completed point 0 :wink:

## Development guidelines
### Coding style

We are following WebKit [Code Style Guideline](https://webkit.org/code-style-guidelines/) for `C++` and [PEP8](https://www.python.org/dev/peps/pep-0008/) standard for Python code.
We also find very useful [C++ Core Guidelines](https://github.com/isocpp/CppCoreGuidelines/) by Bjarne Stroustrup and Herb Sutter and strongly encourage you to read them too.

You can check and fix your code formatting through the usage of `Black`:

``` shell
poetry run black --check -l 100 gemos
```

The CI will also ensure your code is properly formatted before merging any of your changes.

### Documentation writing

We use the [sphinx](https://www.sphinx-doc.org/en/master/usage/quickstart.html) with read-the-docs theme for `python` code documentation.
The additional necessary dependencies are installed as part of the developments packages.

*Note: `nbsphinx` uses [pandoc](https://pandoc.org/installing.html) to convert markdown cells.
If you don't have it installed in your system, you should install it manually.*

The documentation can be generated using the following command:

```shell
poetry run sphinx-build -b html doc dist/doc
```

Generated files are placed in `dist/doc`.

This section will be completed later with precise documentation style guide, for the moment follow the examples from the code itself.

## Running tests

Use [pylint](https://www.pylint.org/) to check if your code quality is good.

``` shell
poetry run pylint gemos
```

Standard linter configuration is used.
Compare the linter ratings before and after your modifications and do not commit code that reduces the code quality.

## Tips
### Poetry

If you are not using the GEM development machines, you can install `poetry` for your user as follow:

```shell
curl -sSL https://install.python-poetry.org | POETRY_VERSION=1.4.2 python3 -
export PATH=$HOME/.local/bin:$PATH
```

Please note that the virtual environments and caches created by `poetry` are stored in your personal home folder.
In some setups, it is desired to move their locations to other directories.
This can be achieved via `poetry` configuration, either per project or global to your user:

``` shell
poetry config [--local] virtualenvs.in-project true # Store the virtual environment under .venv in the project
poetry config [--local] cache-dir <my-path-with-more-storage> # Modify the cache location
```

In order to avoid prefixing all commands with `poetry run`, one can enter into a sub-shell in which the virtual environment is enabled:

``` shell
poetry shell
```

### PostgreSQL

On official GEM development machines, every user is provided a personal database with the following parameters:
```
host: localhost
port: 5432
user: ${USER}
password: ${USER}
database: ${USER}
```

From your account, it is possible to connect directly to your database without any password with `psql`.

#### `psql` cheatsheet

* Quit the client: `\q`
* List the tables in the current database: `\dt+`
