"""GEM Online Analysis Tools"""

from importlib.metadata import version

__version__ = version("cmsgemos-analysis")
