"""Utilities functions related to the VFAT calibration parameters handling"""

import os

import oracledb
import pandas as pd


def add_parameters(df, df_calibration):
    """Function adding the calibration parameters to a DataFrame"""

    return df.join(df_calibration.set_index("chip-id"), on="chip-id")


def dump_database(output_filename):
    """Function retrieving the VFAT calibration parameters from database"""

    # Oracle SQL query retrieving all calibration constants for all VFAT
    SQL_QUERY = "SELECT * FROM CMS_GEM_MUON_VIEW.GEM_VFAT3_PROD_SUMMARY_V_RH"

    # Mapping between the DB data fields and analysis suite fields
    DB_MAPPING = {
        "VFAT3_SER_NUM": "serial-number",
        "VFAT3_BARCODE": "barcode",
        "VFAT_CHIPID": "chip-id",
        "VFAT3_VERSION": "vfat-version",
        "RUN_NUMBER": "revision",
        "VREF_ADC": "vref-adc",
        "IREF": "iref",
        "ADC0M": "adc0-m",
        "ADC0B": "adc0-b",
        "CAL_DACM": "cal-dac-m",
        "CAL_DACB": "cal-dac-b",
        "STATE": "state",
    }

    # Fields, with their associated type, to save in the output file
    OUTPUT_FIELDS = {
        "chip-id": int,
        "vref-adc": int,
        "iref": int,
        "adc0-m": float,
        "adc0-b": float,
        "cal-dac-m": float,
        "cal-dac-b": float,
        "state": str,
        "revision": int,
        "serial-number": str,
        "barcode": int,
        "vfat-version": int,
    }

    print("Retrieving values from DB...")

    # DB access credentials of the form <user>/<password>@<database>
    db_credentials = os.environ["VFAT_DB_CREDENTIALS"]
    gem_db = oracledb.connect(db_credentials, config_dir="/etc")

    df = pd.read_sql(SQL_QUERY, gem_db)

    print("Producing a sane output file...")

    # Keep and rename the fields of interest
    df = df.filter(items=DB_MAPPING.keys())
    df.rename(columns=DB_MAPPING, inplace=True)

    # Select the most recent revision for each chip ID
    df = df[df.groupby(["chip-id"])["revision"].transform(max) == df["revision"]]

    # Sanitize the entries
    df.dropna(inplace=True)  # Missing values
    df = df[OUTPUT_FIELDS.keys()]  # Columns ordering
    for field_name, field_type in OUTPUT_FIELDS.items():
        df[field_name] = df[field_name].apply(field_type)  # Type
    df.sort_values(by=["chip-id"], inplace=True)  # Order by chip ID

    # Warn for duplicated chip ID
    duplicates = df[df.duplicated(subset=["chip-id"], keep=False)]
    if not duplicates.empty:
        print("Warning: Duplicated entries detected!!!")
        print(duplicates)

    # Write output file
    if output_dir := os.path.dirname(output_filename):
        os.makedirs(output_dir, exist_ok=True)
    df.to_csv(output_filename, sep=";", index=False)

    print("VFAT calibration parameters written to {}".format(output_filename))
    return output_filename
