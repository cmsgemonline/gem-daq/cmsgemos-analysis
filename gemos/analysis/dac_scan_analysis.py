"""DAC scan analysis routines"""

import os
import matplotlib.pyplot as plt
import gc
import mplhep
import pandas as pd
import numpy as np
from numpy import ceil, argmin
from enum import Enum
from multiprocessing import cpu_count, Pool

from gemos.analysis.vfat_calibrations import add_parameters

plt.style.use(mplhep.style.CMS)

# Taken directly from the legacy code
# cross-checking with tables 12 and 13 from the VFAT3 manual
NOMINAL_DAC_VALUES = {
    "CFG_BIAS_PRE_I_BIT": (150, "µA"),
    "CFG_BIAS_PRE_I_BLCC": (25e-3, "µA"),
    "CFG_BIAS_PRE_I_BSF": (26, "µA"),
    "CFG_BIAS_SH_I_BFCAS": (26, "µA"),
    "CFG_BIAS_SH_I_BDIFF": (16, "µA"),
    "CFG_BIAS_SD_I_BDIFF": (28, "µA"),
    "CFG_BIAS_SD_I_BFCAS": (27, "µA"),
    "CFG_BIAS_SD_I_BSF": (30, "µA"),
    "CFG_BIAS_CFD_DAC_1": (20, "µA"),
    "CFG_BIAS_CFD_DAC_2": (20, "µA"),
    "CFG_HYST": (100e-3, "µA"),
    "CFG_THR_ARM_DAC": (64, "mV"),
    "CFG_THR_ZCC_DAC": (5.5, "mV"),
    "CFG_BIAS_PRE_VREF": (430, "mV"),
}
NOMINAL_DAC_SCALING_FACTORS = {
    "CFG_BIAS_PRE_I_BIT": 0.2,
    "CFG_BIAS_PRE_I_BLCC": 100,
    "CFG_BIAS_PRE_I_BSF": 0.25,
    "CFG_BIAS_SH_I_BFCAS": 1,
    "CFG_BIAS_SH_I_BDIFF": 1,
    "CFG_BIAS_SD_I_BDIFF": 1,
    "CFG_BIAS_SD_I_BFCAS": 1,
    "CFG_BIAS_SD_I_BSF": 0.25,
    "CFG_BIAS_CFD_DAC_1": 1,
    "CFG_BIAS_CFD_DAC_2": 1,
    "CFG_HYST": 5,
    "CFG_THR_ARM_DAC": 1,
    "CFG_THR_ZCC_DAC": 4,
    "CFG_BIAS_PRE_VREF": 1,
}
NOMINAL_IREF = 10 * 0.5

CHI2_LIMIT_VALUES = {
    "CFG_BIAS_CFD_DAC_1": {
        "q1": 0.0050004859991189,
        "q3": 0.013041709031601224,
        "iqr": 0.008041223032482323,
    },
    "CFG_BIAS_CFD_DAC_2": {
        "q1": 0.0048716763313496995,
        "q3": 0.012736073173760649,
        "iqr": 0.00786439684241095,
    },
    "CFG_BIAS_PRE_I_BIT": {
        "q1": 0.025213989541845748,
        "q3": 0.029582878224088047,
        "iqr": 0.004368888682242299,
    },
    "CFG_BIAS_PRE_I_BLCC": {
        "q1": 1.2282265620548827e-07,
        "q3": 2.1590543985878045e-07,
        "iqr": 9.308278365329218e-08,
    },
    "CFG_BIAS_PRE_I_BSF": {
        "q1": 0.0338969292911463,
        "q3": 0.07277252765421435,
        "iqr": 0.03887559836306805,
    },
    "CFG_BIAS_PRE_VREF": {
        "q1": 2.4039539611000236,
        "q3": 5.080335352880438,
        "iqr": 2.676381391780414,
    },
    "CFG_BIAS_SD_I_BDIFF": {
        "q1": 0.003990295954956076,
        "q3": 0.007840633893967774,
        "iqr": 0.003850337939011699,
    },
    "CFG_BIAS_SD_I_BFCAS": {
        "q1": 0.004516684039551075,
        "q3": 0.0091177071403617,
        "iqr": 0.0046010231008106256,
    },
    "CFG_BIAS_SD_I_BSF": {
        "q1": 0.030940870225743572,
        "q3": 0.06477680835591645,
        "iqr": 0.03383593813017288,
    },
    "CFG_BIAS_SH_I_BDIFF": {
        "q1": 0.004087893923935425,
        "q3": 0.00826365358254875,
        "iqr": 0.004175759658613325,
    },
    "CFG_BIAS_SH_I_BFCAS": {
        "q1": 0.00431426321590375,
        "q3": 0.008426962686068075,
        "iqr": 0.004112699470164325,
    },
}

POLYNOMIAL_DEGREE = 5


class PlottingCondition(Enum):
    ALWAYS = "always"
    NEVER = "never"
    WARNING = "warning"

    def __str__(self):
        return self.value


def apply_adc_calibration(df, calibration_df):
    """Convert the DAC scan ADC measureents into DAC voltages and current"""

    # Use indexing for the computations
    df = df.set_index(["fed", "slot", "oh", "vfat"]).sort_index()
    calibration_df = calibration_df.set_index(["fed", "slot", "oh", "vfat"]).sort_index()

    # Apply the ADC conversion
    # Note that df and calibration_df might have different indexes
    # Drop any missing value, because of missing data or because of missing calibration
    df["adc-value"] = df["adc-value"].multiply(calibration_df["adc0-m"]).dropna()
    df["adc-value"] = df["adc-value"].add(calibration_df["adc0-b"]).dropna()

    # Divide by the 20 kΩ resistor if the nominal register value is expressed in µA
    is_current_dac = df["dac-name"].map(lambda x: NOMINAL_DAC_VALUES[x][1]) == "µA"
    df.loc[is_current_dac, "adc-value"] /= 20
    df.loc[is_current_dac, "adc-value"] -= NOMINAL_IREF

    # Apply the scaling factors
    df["adc-value"] /= df["dac-name"].map(NOMINAL_DAC_SCALING_FACTORS)

    return df.reset_index()


def find_dac_scan_result(input_df):
    """
    Find the best DAC value based on the nominal ADC recommended for a given DAC circuit.
    Check if the ADC recommended is in included in the range scanned, otherwise an out of range warning is issued.
    Check if the ADC vs DAC curve is monotonic (as it should be), otherwise a not monotonic  warning is issued.
    Check if the ADC vs DAC curve is describable with a polynomial functional form (mainly to detect brocken DAC circuits), otherwise a bad fit warning is issued.
    Flag a generic warning if any of the three above is raised.
    """

    output_list = []

    # Determine DAC value for each register
    for (fed, slot, oh, vfat, dac_name), dac_df in input_df.groupby(
        ["fed", "slot", "oh", "vfat", "dac-name"]
    ):
        dac_df = dac_df.reset_index(drop=True)
        adc_values, dac_values = dac_df["adc-value"].to_numpy(), dac_df["dac-value"].to_numpy()
        nominal_value = NOMINAL_DAC_VALUES[dac_name][0]

        # Calculate chosen ADC value as closest to nominal DAC value
        chosen_index = argmin(abs(adc_values - nominal_value))
        chosen_dac = dac_values[chosen_index]

        # Checking for possible issue in the DAC scans
        # First check out of range ADC
        out_of_range_warning = False
        if (nominal_value <= min(adc_values)) or (nominal_value >= max(adc_values)):
            out_of_range_warning = True
            print(
                f"Warning: target value for {dac_name} outside of DAC range, value set to {chosen_dac} for {fed, slot, oh, vfat}"
            )

        # Check for non monotic scans
        adc_diffs = np.diff(adc_values)
        monoton = adc_diffs[(adc_diffs < 0)]
        not_monotonic_warning = False
        if monoton.size > 0:
            not_monotonic_warning = True

        # Check for scans with poor functional fit. Fit performed with a polynomial of a POLYNOMIAL_DEGREE degree.
        p, residuals, rank, singular_values, rcond = np.polyfit(
            dac_values, adc_values, POLYNOMIAL_DEGREE, full=True
        )
        ndof = len(dac_values) - POLYNOMIAL_DEGREE - 1
        chi2 = residuals / ndof
        bad_fit_warning = False

        # Compare the chi2 with some reference values for the goodness of the fit.
        # The reference values are established by the IQ ranges of the chi2 distributions for each DAC scan
        chi2_cut = CHI2_LIMIT_VALUES[dac_name]["q3"] + (4 * CHI2_LIMIT_VALUES[dac_name]["iqr"])
        if chi2 > chi2_cut:
            bad_fit_warning = True

        # Generic warning if any of the three is raised
        generic_warning = out_of_range_warning or not_monotonic_warning or bad_fit_warning

        vfat_df_row = {
            "fed": fed,
            "slot": slot,
            "oh": oh,
            "vfat": vfat,
            "dac-name": dac_name,
            "dac-value": chosen_dac,
            "out-of-range-warning": out_of_range_warning,
            "bad-fit-warning": bad_fit_warning,
            "not-monotonic-warning": not_monotonic_warning,
            "warning": generic_warning,
            "residual": residuals[0],
            "chi2": chi2[0],
        }
        for i in range(0, POLYNOMIAL_DEGREE + 1):
            vfat_df_row["p{}".format(i)] = p[i]

        output_list.append(vfat_df_row)

    return pd.DataFrame.from_records(output_list)


def plot_scan(vfat_group, output_df, output_dir):
    """Plot all DAC rate scans"""

    (fed, slot, oh, vfat), vfat_df = vfat_group

    dac_group = vfat_df.groupby("dac-name")

    # Prepare figure for plotting
    nrows, ncols = 3, int(ceil(len(dac_group) / 3))
    dac_scan_fig, dac_scan_axs = plt.subplots(nrows, ncols, figsize=(55, 25))
    dac_scan_axs = dac_scan_axs.flat

    # Plot all DAC
    for group_index, (dac_name, dac_df) in enumerate(dac_group):

        # Plot DAC scan for current VFAT and register
        dac_scan_axs[group_index].set_xlabel("DAC units")
        dac_scan_axs[group_index].set_ylabel(f"{dac_name} ({NOMINAL_DAC_VALUES[dac_name][1]})")

        dac_scan_axs[group_index].plot(dac_df["dac-value"], dac_df["adc-value"], ".", color="black")

        # Draw line pointing to set DAC value
        nominal_value = NOMINAL_DAC_VALUES[dac_name][0]
        chosen_dac = output_df.loc[(fed, slot, oh, vfat, dac_name), "dac-value"]

        # Plot fitted curve
        p = []
        for i in range(0, POLYNOMIAL_DEGREE + 1):
            p.append(output_df.loc[(fed, slot, oh, vfat, dac_name), "p{}".format(i)])

        chi2 = output_df.loc[(fed, slot, oh, vfat, dac_name), "chi2"]
        chi2_cut = CHI2_LIMIT_VALUES[dac_name]["q3"] + (4 * CHI2_LIMIT_VALUES[dac_name]["iqr"])

        # Use a different color and text to display DAC scans affected by warnings
        plot_color = "xkcd:white"
        warning_text = ""
        if output_df.loc[(fed, slot, oh, vfat, dac_name), "out-of-range-warning"]:
            plot_color = "xkcd:pastel blue"
            warning_text = "out of range"
        elif output_df.loc[(fed, slot, oh, vfat, dac_name), "bad-fit-warning"]:
            plot_color = "xkcd:salmon"
            warning_text = "$\chi^2 = {:.8f}$\n cut = {:.8f}".format(chi2, chi2_cut)
        elif output_df.loc[(fed, slot, oh, vfat, dac_name), "not-monotonic-warning"]:
            plot_color = "xkcd:celadon"
            warning_text = "not monotonic"

        dac_scan_axs[group_index].set_facecolor(plot_color)
        dac_scan_axs[group_index].text(
            0.6,
            0.6,
            warning_text,
            fontsize=26,
            horizontalalignment="left",
            verticalalignment="bottom",
            transform=dac_scan_axs[group_index].transAxes,
        )

        # Plot the fitted function
        x = dac_df["dac-value"].to_numpy()
        y = np.polyval(p, x)
        dac_scan_axs[group_index].plot(x, y, color="red")

        # Plot the nominal ADC and the chosen DAC point
        dac_scan_axs[group_index].plot(
            [chosen_dac, chosen_dac],
            [min(dac_df["adc-value"]), nominal_value],
            "--",
            color="blue",
        )
        dac_scan_axs[group_index].plot(
            [min(dac_df["dac-value"]), chosen_dac],
            [nominal_value, nominal_value],
            "--",
            color="blue",
        )

    # Save the figure
    output_figure = "{}/fed{}-slot{}-oh{}-vfat{}.png".format(output_dir, fed, slot, oh, vfat)
    dac_scan_fig.savefig(output_figure)
    print("Scan plot saved to {}".format(output_figure))

    # Make sure to keep the memory usage reasonable
    plt.close(dac_scan_fig)
    gc.collect()


def create_configuration(
    input_filenames,
    output_dir,
    mapping_filename,
    calibration_filename,
    plotting=PlottingCondition.ALWAYS,
):
    # Load the input files
    input_df = pd.concat((pd.read_csv(f, sep=";") for f in input_filenames), ignore_index=True)
    calibration_df = add_parameters(
        pd.read_csv(mapping_filename, sep=";"), pd.read_csv(calibration_filename, sep=";")
    )

    # Apply calibrations to the ADC
    input_df = apply_adc_calibration(input_df, calibration_df)

    # Find the best values for a given DAC and check the scan for possible problems with the DAC circuit
    output_df = find_dac_scan_result(input_df)

    # Save the results in a single output file
    os.makedirs(output_dir, exist_ok=True)
    output_filename = output_dir / "dac-values.dat"
    output_df.to_csv(output_filename, sep=";", index=False)
    print("Output DAC values written to {}".format(output_filename))

    # Plot the DAC scans
    if plotting != PlottingCondition.NEVER:
        # Select only problematic VFAT
        if plotting == PlottingCondition.WARNING:
            # List all VFAT with a warning condition
            warning_vfat = output_df.loc[
                ((output_df["warning"] == True)),
                ["fed", "slot", "oh", "vfat"],
            ].drop_duplicates()
            warning_vfat = warning_vfat.to_numpy().tolist()

            # Keep only those VFAT
            input_df = (
                input_df.set_index(["fed", "slot", "oh", "vfat"]).loc[warning_vfat].reset_index()
            )
            output_df = (
                output_df.set_index(["fed", "slot", "oh", "vfat"]).loc[warning_vfat].reset_index()
            )

        # Plot!
        os.makedirs("{}/plots".format(output_dir), exist_ok=True)

        output_df = output_df.set_index(["fed", "slot", "oh", "vfat", "dac-name"]).sort_index()

        pool = Pool(cpu_count())

        for vfat_group in input_df.groupby(["fed", "slot", "oh", "vfat"]):
            pool.apply_async(
                plot_scan,
                args=(
                    vfat_group,
                    output_df,
                    output_dir / "plots",
                ),
                error_callback=lambda e: print(
                    "Error: " + repr(e)  # Just display the error for now
                ),
            )

        pool.close()
        pool.join()

    return output_filename
