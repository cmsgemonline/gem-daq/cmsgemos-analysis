"""S-curves analysis routines"""

from enum import Enum, IntEnum
from multiprocessing import cpu_count, Pool
from re import findall
from warnings import catch_warnings
import gc
import os

from matplotlib.cbook import boxplot_stats
from matplotlib.colors import Normalize
from mplhep import style
from scipy.optimize import curve_fit
from scipy.special import erf
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import numpy as np
import pandas as pd
import uproot

from gemos.analysis.vfat_calibrations import add_parameters

plt.style.use(style.CMS)
scurve_sp_cmap = plt.get_cmap("viridis").copy()  # cmap for the summary 2D plot
scurve_sp_cmap.set_under("w")
scurve_sp_norm = Normalize(vmin=0.25, vmax=100, clip=False)  # Normalization for the summary 2D plot


class PlottingMode(Enum):
    ESSENTIALS = "essentials"
    FULL = "full"
    NO_PLOT = "no-plot"

    def __str__(self):
        return self.value


class Units(Enum):
    DAC_UNITS = "DAC"
    FC = "fC"

    def __str__(self):
        return self.value


class FitStatus(IntEnum):
    NO_FIT = -1
    FAILURE = 0
    SUCCESS = 1
    WARNING = 2


def _sigmoid(x, mean=0.01, sigma=0.01):
    """Sigmoid function to fit the S-curves."""
    return 0.5 + 0.5 * erf(-(x - mean) / (np.sqrt(2) * sigma))


def _show_report(report_info):
    print(
        "=================================================================\n"
        + "                  Final scurve analysis report\n"
        + "=================================================================\n"
    )

    for info in report_info:
        print("For ", info["filename"])
        print("------------------------------------------------------------")
        print("{} - OH analyzed.".format(info["analyzed oh"]))
        print("{} - VFAT analyzed.".format(info["analyzed vfat"]))
        print("{} - empty channels found.".format(info["empty channels"]))
        print("{} - channels failed fitting.".format(info["fit-failed channels"]))
        print("{} - channels warning fitting.".format(info["fit-warning channels"]))
        print("{} - noisy channels.".format(info["noisy channels"]))
        print("------------------------------------------------------------")


def _analyze_channel(ch_data):
    """Routine to fit and extract the parameters for a single channel

    :param list ch_data: list with the channel information.

    :return tuple: An array with optimal values for the parameters and a status flag that indicates if the fit was successful or not.
    """
    # Do not analyze empty channels
    if (ch_data == 0).all():
        return [None, None], FitStatus.NO_FIT

    x = range(len(ch_data))
    # Data is normalized to avoid covariance estimation warnings
    y = np.array(ch_data) / ch_data.max()

    with catch_warnings(record=True) as w:
        try:
            popt, _, _, errmsg, ierr = curve_fit(_sigmoid, x, y, p0=[230, 2], full_output=True)

        except RuntimeError:
            return [None, None], FitStatus.FAILURE

        if not (1 <= ierr <= 4):
            return [None, None], FitStatus.FAILURE

        if w:
            return popt, FitStatus.WARNING

    return popt, FitStatus.SUCCESS


def _analyze_vfat(vfat_dir):
    """Routine to analyze all the channels in a VFAT, the resulting information is returned as a
    pandas DataFrame.

    :param uproot.reading.ReadOnlyDirectory vfat_dir: groupby object with the VFAT information.

    :return pandas.DataFrame: DataFrame with the computed information.
    """
    fed, slot, oh, vfat = [int(i) for i in findall(r"\d+", vfat_dir.object_path)]

    values = []
    for ch, ch_data in enumerate(vfat_dir["chanHitvsCalDAC2D"].values().T):
        popt, status = _analyze_channel(ch_data)
        mean, sigma = popt

        if status == FitStatus.WARNING:
            print("Fit warning for channel {}-{}-{}-{}-{}".format(fed, slot, oh, vfat, ch))
        if status == FitStatus.FAILURE:
            print("Fit failed for channel {}-{}-{}-{}-{}".format(fed, slot, oh, vfat, ch))

        values.append(
            {
                "fed": fed,
                "slot": slot,
                "oh": oh,
                "vfat": vfat,
                "channel": ch,
                "fit-status": status,
                "threshold": mean,
                "enc": sigma,
                "empty": status == FitStatus.NO_FIT,
                "noisy": None,
            }
        )

    return pd.DataFrame.from_records(values)


def _filter_noisy_channels(enc_data, whis):
    """Routine to filter noisy channels based on the outliers points defined by the interquartile range.
    Only upper fliers are taken.

    :param pandas.core.series.Series enc_data: Pandas series with the noise of the channels for a VFAT
    :param float whis: The position of the whiskers. e.g. The lower whisker is at the lowest datum above Q1 - whis*(Q3-Q1)
    """
    stats = boxplot_stats(enc_data.dropna(), whis=whis)[0]  # failed and empty data are removed
    return (enc_data.isin(stats["fliers"])) & (enc_data.gt(stats["med"]))


def _scurve_summary_plots(oh_dirs, graph_name, units=Units.DAC_UNITS, calibration_df=None):
    """Routine to produce a summary 2D plots of the VFATs for an OH

    :param list of uproot.reading.ReadOnlyDirectory oh_dirs: list with the informations of all the vfats of a single oh.
    :param pathlib.PosixPath graph_name: Path to save the images.
    :param Units object: Data units {"DAC", "fC"}, it is necessary to change to desired units.
    :param pandas.DataFrame calibration_df: Pandas DataFrame with the calibration info.
    """
    fig, axs = plt.subplots(3, 8, figsize=(55, 25))
    axs = axs.flat
    displayed = []

    for vfat_dir in oh_dirs:
        # A map per VFAT is built
        fed, slot, oh, vfat = [int(i) for i in findall(r"\d+", vfat_dir.object_path)]

        # Displayed VFAT is saved to register which ones are missing
        displayed.append(vfat)

        histo = vfat_dir["chanHitvsCalDAC2D"].values()

        im = axs[vfat].imshow(
            histo,
            aspect=128.0 / 255.0,
            cmap=scurve_sp_cmap,
            norm=scurve_sp_norm,
            origin="upper" if units == Units.FC else "lower",
            interpolation="none",
        )
        axs[vfat].set_title("VFAT {}".format(vfat), fontsize=18)
        axs[vfat].set_xlabel("VFAT Channel", fontsize=13)
        axs[vfat].set_ylabel("Injected charge [{}]".format(units), fontsize=13)
        axs[vfat].tick_params(axis="x", labelsize=10)
        axs[vfat].tick_params(axis="y", labelsize=10)
        cbar = fig.colorbar(im, ax=axs[vfat], pad=0.01, fraction=0.048)

        if units == Units.FC:
            # If fC units, the y axis must be changed
            m, b = calibration_df.loc[
                (calibration_df["fed"] == fed)
                & (calibration_df["slot"] == slot)
                & (calibration_df["oh"] == oh)
                & (calibration_df["vfat"] == vfat),
                ["cal-dac-m", "cal-dac-b"],
            ].values[0]

            x_ticks = np.array(axs[vfat].get_yticks()) * m + b

            ticks_loc = axs[vfat].get_yticks().tolist()
            axs[vfat].yaxis.set_major_locator(mticker.FixedLocator(ticks_loc))
            axs[vfat].set_yticklabels(["{:,.0f}".format(tick) for tick in x_ticks])

    # Empty plots are produced according to displayed list
    for vfat in [i for i in range(24) if i not in displayed]:
        axs[vfat].imshow(
            np.zeros((255, 128)),
            aspect=128.0 / 255.0,
            cmap=scurve_sp_cmap,
            norm=scurve_sp_norm,
            origin="lower",
        )
        axs[vfat].text(64, 124, "NO DATA", ha="center", va="center", rotation=45, fontsize=30)
        axs[vfat].set_title("VFAT {}".format(vfat), fontsize=18)
        axs[vfat].set_xticks([])
        axs[vfat].set_yticks([])

    plt.tight_layout()
    fig.savefig(graph_name)

    print("S-curve summary plot saved to", graph_name, flush=True)

    # Make sure to keep the memory usage reasonable
    plt.close(fig)
    gc.collect()


def _scurve_boxplots(oh_df, graph_name, units=Units.DAC_UNITS):
    """Routine to produce a box plot with the activation thresholds and a box plot with the noise/ENC

    :param pandas.DataFrameGroupBy oh_df: OH DataFrame with the informations to produce plots.
    :param pathlib.PosixPath graph_name: Path to save the images.
    :param Units object: Data units {"DAC", "fC"}, it is necessary just to put in the labels.
    """
    fed, slot, oh = [int(i) for i in findall(r"\d+", graph_name.name)]

    # Missing vfats are filled to complete the boxplot
    included = oh_df["vfat"].drop_duplicates()
    missings = [i for i in range(24) if i not in included.to_list()]

    for vfat in missings:
        aux = pd.DataFrame(
            {
                "fed": fed,
                "slot": slot,
                "oh": oh,
                "vfat": vfat,
                "channel": range(128),
                "fit-status": [None] * 128,
                "threshold": [None] * 128,
                "enc": [None] * 128,
                "empty": [None] * 128,
            }
        )
        oh_df = pd.concat([oh_df, aux], ignore_index=True)

    fig, axs = plt.subplots(1, 2, figsize=(18, 8))

    # box plot of the activation thresholds
    oh_df.boxplot(
        column="threshold",
        by="vfat",
        ax=axs[0],
        showmeans=True,
        meanline=True,
    )
    axs[0].set_title("")
    axs[0].set_xlabel("VFAT", fontsize=15)
    axs[0].set_ylabel(r"Threshold - $\mu$ [{}]".format(units), fontsize=15)
    axs[0].tick_params(labelsize=12)

    # box plot of the noise(ENC)
    oh_df.boxplot(
        column="enc",
        by="vfat",
        ax=axs[1],
        showmeans=True,
        meanline=True,
    )
    axs[1].set_title("")
    axs[1].set_xlabel("VFAT", fontsize=15)
    axs[1].set_ylabel(r"Noise (ENC) - $\sigma$ [{}]".format(units), fontsize=15)
    axs[1].tick_params(labelsize=12)

    plt.tight_layout()
    fig.savefig(graph_name)
    print("S-curve boxplots saved to", graph_name, flush=True)

    # Make sure to keep the memory usage reasonable
    plt.close(fig)
    gc.collect()


def _scurve_channels_distribution_plot(vfat_df, graph_name, units=Units.DAC_UNITS, whis=1.5):
    """Routine to produce histograms of the threshold and noise/ENC values computed for a VFAT.

    :param pandas.DataFrameGroupBy vfat_df: VFAT dataFrame with the informations to produce plots.
    :param pathlib.PosixPath graph_name: Path to save the images.
    :param Units object: Data units {"dac", "fc"}, it is necessary to change to desired units.
    :param float whis: The position of the whiskers. e.g. The lower whisker is at the lowest datum above Q1 - whis*(Q3-Q1)
    """
    fig, axs = plt.subplot_mosaic(
        [
            ["A", "A", "B"],
            ["C", "C", "D"],
        ],
        figsize=(17, 10),
    )

    if vfat_df["empty"].eq(1).all():
        axs["A"].text(0.5, 0.5, "NO DATA", ha="center", va="center", rotation=45, fontsize=30)
        axs["B"].text(0.5, 0.5, "NO DATA", ha="center", va="center", rotation=45, fontsize=30)
        axs["C"].text(0.5, 0.5, "NO DATA", ha="center", va="center", rotation=45, fontsize=30)
        axs["D"].text(0.5, 0.5, "NO DATA", ha="center", va="center", rotation=45, fontsize=30)

        plt.tight_layout()
        fig.savefig(graph_name)
        print("Complete VFAT plot saved to", graph_name, flush=True)

        # Make sure to keep the memory usage reasonable
        plt.close(fig)
        gc.collect()

        return 0

    statistics_of_thr = boxplot_stats(vfat_df["threshold"].dropna(), whis=whis)[0]
    statistics_of_enc = boxplot_stats(vfat_df["enc"].dropna(), whis=whis)[0]

    axs["A"].scatter(vfat_df["channel"], vfat_df["threshold"], s=3, color="darkblue")
    axs["A"].hlines(statistics_of_thr["mean"], 0, 127, color="r", linestyle="--")
    axs["A"].hlines(statistics_of_thr["med"], 0, 127, color="r")
    axs["A"].fill_between(
        range(128),
        statistics_of_thr["q1"],
        statistics_of_thr["q3"],
        color="r",
        alpha=0.15,
    )
    axs["A"].fill_between(
        range(128),
        statistics_of_thr["whislo"],
        statistics_of_thr["whishi"],
        color="r",
        alpha=0.025,
    )
    axs["A"].grid(True)
    axs["A"].set_xlabel("Channel", fontsize=14)
    axs["A"].set_ylabel(r"Activation Threshold - $\mu$ [{}]".format(units), fontsize=14)
    axs["A"].set_xticks(range(0, 121, 20))
    axs["A"].set_xticklabels(range(0, 121, 20))
    axs["A"].tick_params(labelsize=12)

    n, _, _ = axs["B"].hist(
        vfat_df["threshold"],
        bins=40,
        edgecolor="black",
        color="darkblue",
        orientation="horizontal",
        alpha=0.6,
    )
    axs["B"].hlines(
        statistics_of_thr["mean"],
        0,
        max(n),
        color="r",
        linestyle="--",
        label="mean = {:.3f}".format(statistics_of_thr["mean"]),
    )
    axs["B"].hlines(
        statistics_of_thr["med"],
        0,
        max(n),
        color="r",
        label="median = {:.3f}".format(statistics_of_thr["med"]),
    )
    axs["B"].fill_between(
        range(int(max(n))),
        statistics_of_thr["q1"],
        statistics_of_thr["q3"],
        color="r",
        alpha=0.15,
        label=r"iqr = {:.3f}".format(statistics_of_thr["iqr"]),
    )
    axs["B"].fill_between(
        range(int(max(n))),
        statistics_of_thr["whislo"],
        statistics_of_thr["whishi"],
        color="r",
        alpha=0.025,
        label=r"whiskers = {:.3f}".format(
            statistics_of_thr["whishi"] - statistics_of_thr["whislo"]
        ),
    )
    axs["B"].yaxis.tick_right()
    axs["B"].set_xlabel("Ocurrence", fontsize=14)
    axs["B"].tick_params(labelsize=12)

    axs["C"].scatter(vfat_df["channel"], vfat_df["enc"], s=3, color="darkred")
    axs["C"].hlines(statistics_of_enc["mean"], 0, 127, linestyle="--", color="r")
    axs["C"].hlines(statistics_of_enc["med"], 0, 127, color="r")
    axs["C"].fill_between(
        range(128),
        statistics_of_enc["q1"],
        statistics_of_enc["q3"],
        color="r",
        alpha=0.1,
    )
    axs["C"].fill_between(
        range(128),
        statistics_of_enc["whislo"],
        statistics_of_enc["whishi"],
        color="r",
        alpha=0.05,
    )
    axs["C"].grid(True)
    axs["C"].set_xlabel("Channel", fontsize=14)
    axs["C"].set_ylabel(r"Noise (ENC) - $\sigma$ [{}]".format(units), fontsize=14)
    axs["C"].set_xticks(range(0, 121, 20))
    axs["C"].set_xticklabels(range(0, 121, 20))
    axs["C"].tick_params(labelsize=12)

    n, _, _ = axs["D"].hist(
        vfat_df["enc"],
        bins=40,
        edgecolor="black",
        color="darkred",
        orientation="horizontal",
        alpha=0.6,
    )
    axs["D"].hlines(
        statistics_of_enc["mean"],
        0,
        max(n),
        color="r",
        label="mean = {:.3f}".format(statistics_of_enc["mean"]),
    )
    axs["D"].hlines(
        statistics_of_enc["med"],
        0,
        max(n),
        color="r",
        linestyle="--",
        label="median = {:.3f}".format(statistics_of_enc["med"]),
    )
    axs["D"].fill_between(
        range(int(max(n))),
        statistics_of_enc["q1"],
        statistics_of_enc["q3"],
        color="r",
        alpha=0.1,
        label=r"iqr = {:.3f}".format(statistics_of_enc["iqr"]),
    )
    axs["D"].fill_between(
        range(int(max(n))),
        statistics_of_enc["whislo"],
        statistics_of_enc["whishi"],
        color="r",
        alpha=0.05,
        label=r"whiskers = {:.3f}".format(
            statistics_of_enc["whishi"] - statistics_of_enc["whislo"]
        ),
    )
    axs["D"].yaxis.tick_right()
    axs["D"].set_xlabel("Ocurrence", fontsize=14)
    axs["D"].tick_params(labelsize=12)

    fig.suptitle("Channels distributuion of the s-curve results", fontsize=25)

    axs["B"].legend(bbox_to_anchor=(1.1, 0.6), loc="lower left", fontsize=16)
    axs["D"].legend(bbox_to_anchor=(1.1, 0.6), loc="lower left", fontsize=16)

    plt.tight_layout()
    fig.savefig(graph_name)
    print("Complete VFAT plot saved to", graph_name, flush=True)

    # Make sure to keep the memory usage reasonable
    plt.close(fig)
    gc.collect()


def create_configuration(
    input_filenames,
    output_dir,
    mapping_filename=None,
    calibration_filename=None,
    plotting=PlottingMode.ESSENTIALS,
    units=Units.DAC_UNITS,
    full_output=False,
    noise_iqr=1.5,
):
    """Routine to perform the analysis"""

    # Check if the files to change units are provided
    if units == Units.FC and not (mapping_filename and calibration_filename):
        print("When fC units is specified, calibration and mapping file must also be provided.")
        return 0

    print("\n..................Starting S-curve analysis..................\n")

    report_info = []

    # Initialize the output directory
    os.makedirs(output_dir, exist_ok=True)

    with uproot.recreate(output_dir / "scurve-results.root") as output_file:
        output_file.mktree(
            "scurve-results",
            {
                "fed": int,
                "slot": int,
                "oh": int,
                "vfat": int,
                "channel": int,
                "fit-status": int,
                "threshold": float,
                "enc": float,
                "empty": int,
                "noisy": int,
            },
        )

        # For every input files
        for input_file_name in input_filenames:
            # Variables for final console report
            analyzed_vfat = 0
            analyzed_oh = 0
            empty_channels = 0
            fit_failed_channels = 0
            noisy_channels = 0

            print("Analyzing", input_file_name)
            input_file = uproot.open(input_file_name)

            # Running the analysis...
            print("Computing S-curve parameters...")
            pool = Pool(cpu_count())
            results = []

            # Data for every VFAT is stored a ReadOnlyDirectory
            for _, vfat_dir in input_file.items(recursive=False):
                results.append(
                    pool.apply_async(
                        _analyze_vfat,
                        args=(vfat_dir,),
                        error_callback=lambda e: print(
                            "Error: " + repr(e)  # Just display the error for now
                        ),
                    ),
                )
                analyzed_vfat += 1

            # Merge results into a single DataFrame
            final_data = pd.concat([r.get() for r in results], ignore_index=True)

            # Noisy channels are filtered
            final_data["noisy"] = final_data.groupby(["fed", "slot", "oh", "vfat"])[
                "enc"
            ].transform(_filter_noisy_channels, noise_iqr)

            # Data are changed to desired units
            calibration_df = None
            if units == Units.FC:
                print("Converting units...")

                calibration_df = add_parameters(
                    pd.read_csv(mapping_filename, sep=";"),
                    pd.read_csv(calibration_filename, sep=";"),
                )

                final_data = final_data.set_index(["fed", "slot", "oh", "vfat"]).sort_index()
                calibration_df = calibration_df.set_index(
                    ["fed", "slot", "oh", "vfat"]
                ).sort_index()

                final_data["enc"] = final_data["enc"].multiply(
                    np.abs(calibration_df.reindex(final_data.index, method="ffill")["cal-dac-m"])
                )
                final_data["threshold"] = final_data["threshold"].multiply(
                    calibration_df.reindex(final_data.index, method="ffill")["cal-dac-m"]
                )
                final_data["threshold"] = final_data["threshold"].add(
                    calibration_df.reindex(final_data.index, method="ffill")["cal-dac-b"]
                )

                final_data = final_data.reset_index()
                calibration_df = calibration_df.reset_index()

            # Plotting...
            if plotting != PlottingMode.NO_PLOT:
                print("Plotting...")

                # Creating the the output directory
                os.makedirs("{}/plots".format(output_dir), exist_ok=True)

                # One image per OptoHybrid is produced
                for (fed, slot, oh), oh_df in final_data.groupby(["fed", "slot", "oh"]):
                    oh_dirs = [
                        dir_name
                        for _, dir_name in input_file.items(
                            filter_name="fed{}-slot{}-oh{}-*".format(fed, slot, oh),
                            recursive=False,
                        )
                    ]

                    if plotting == PlottingMode.ESSENTIALS or plotting == PlottingMode.FULL:
                        pool.apply_async(
                            _scurve_summary_plots,
                            args=(
                                oh_dirs,
                                output_dir
                                / "plots/scurve-summary-fed{}-slot{}-oh{}.png".format(
                                    fed, slot, oh
                                ),
                                units,
                                calibration_df,
                            ),
                            error_callback=lambda e: print(
                                "Error: " + repr(e)  # Just display the error for now
                            ),
                        )

                    if plotting == PlottingMode.ESSENTIALS or plotting == PlottingMode.FULL:
                        pool.apply_async(
                            _scurve_boxplots,
                            args=(
                                oh_df,
                                output_dir
                                / "plots/scurve-boxplots-fed{}-slot{}-oh{}.png".format(
                                    fed, slot, oh
                                ),
                                units,
                            ),
                            error_callback=lambda e: print(
                                "Error: " + repr(e)  # Just display the error for now
                            ),
                        )

                    if plotting == PlottingMode.FULL:
                        for vfat, vfat_df in oh_df.groupby("vfat"):
                            pool.apply_async(
                                _scurve_channels_distribution_plot,
                                args=(
                                    vfat_df,
                                    output_dir
                                    / "plots/scurve-channels-distribution-fed{}-slot{}-oh{}-vfat{}.png".format(
                                        fed, slot, oh, vfat
                                    ),
                                    units,
                                    noise_iqr,
                                ),
                                error_callback=lambda e: print(
                                    "Error: " + repr(e)  # Just display the error for now
                                ),
                            )

                pool.close()
                pool.join()

            # Final information is saved
            print("Saving results...")
            output_file["scurve-results"].extend(final_data.to_dict("list"))

            # Input information is included just if full_output is indicated
            if full_output:
                output_file.copy_from(input_file)

            # To count how many OH were analyzed
            analyzed_oh = len(final_data.drop_duplicates(["fed", "slot", "oh"]))

            # To identify how many channels without data exist
            empty_channels = len(
                final_data.loc[final_data["empty"] == True].drop_duplicates(
                    ["fed", "slot", "oh", "vfat", "channel"]
                )
            )

            # To identify how many channels were not fitted successfully
            fit_failed_channels = len(
                final_data.loc[final_data["fit-status"] == FitStatus.FAILURE].drop_duplicates(
                    ["fed", "slot", "oh", "vfat", "channel"]
                )
            )

            # To identify how many channels were not fitted because of a warning
            fit_warning_channels = len(
                final_data.loc[final_data["fit-status"] == FitStatus.WARNING].drop_duplicates(
                    ["fed", "slot", "oh", "vfat", "channel"]
                )
            )

            # To count how many noisy channels exist
            noisy_channels = len(
                final_data.loc[(final_data["noisy"] == True)].drop_duplicates(
                    ["fed", "slot", "oh", "vfat", "channel"]
                )
            )

            report_info += [
                {
                    "filename": input_file_name,
                    "analyzed oh": analyzed_oh,
                    "analyzed vfat": analyzed_vfat,
                    "empty channels": empty_channels,
                    "fit-failed channels": fit_failed_channels,
                    "fit-warning channels": fit_warning_channels,
                    "noisy channels": noisy_channels,
                }
            ]

    _show_report(report_info)
    print("Output file saved to " + str(output_dir / "scurve-results.root"))
    print("Done!")
