"""Cluster mask rate scan routines"""

import gc
import os
from multiprocessing import cpu_count, Pool

import pandas as pd
import matplotlib.pyplot as plt
import mplhep

plt.style.use(mplhep.style.CMS)


def _plot_scan_oh(output_directory, df, df_condition):
    """
    Plot cluster rates for single OptoHybrid: masked, unmasked,
    and the delays for which there is a gain.
    """
    nrows, ncols = 1, 1
    fig, ax = plt.subplots(nrows, ncols, figsize=(35, 16))

    def plot_scan(df, ax):
        df.reset_index()
        ax.plot(
            df["delay"],
            df["cluster-rate-unmasked"],
            "o",
            markersize=15,
            color="blue",
            label="Cluster rate w/o mask",
        )
        ax.plot(
            df["delay"],
            df["cluster-rate-masked"],
            "x",
            markersize=20,
            markeredgewidth=3,
            color="green",
            label="Cluster rate w/ mask",
        )

        # Visualize the delays to be masked with a vertical band
        for _, row in df_condition.iterrows():
            delay = row["delay"]
            ax.axvspan(delay - 0.5, delay + 0.5, alpha=0.25, color="red")

        # Style the plot with legend and limits on the axes
        plt.legend(loc="upper left")
        top = df["cluster-rate-unmasked"].max() * 1.5
        ax.set_ylim(bottom=0, top=top)
        ax.set_xlim(-3, 33)
        ax.set_xlabel("Delay (BX)")
        ax.set_ylabel("Cluster rate (Hz)")

    fed = df.iloc[0]["fed"]
    slot = df.iloc[0]["slot"]
    oh = df.iloc[0]["oh"]

    output_figure = "{}/fed{}-slot{}-oh{}.png".format(output_directory, fed, slot, oh)
    plot_scan(df, ax)
    fig.savefig(output_figure)
    print("Scan plot saved to {}".format(output_figure))

    # Make sure to keep the memory usage reasonable
    plt.close(fig)
    gc.collect()


def analyze_scan(input_filenames, output_directory, plotting=True, cut=0.05):
    # Load the input files
    df = pd.concat((pd.read_csv(f, sep=";", dtype=int) for f in input_filenames), ignore_index=True)

    # For each OptoHybrid, apply the following algorithm:
    #
    # 1. Subtract the expected cluster rate drop due to the masking. In the
    #    absence of high-multiplicity events, the clusters due to random noise
    #    will be ignored with a probability of 1/L1A period. No rate drop is
    #    expected from in-time clusters due to the L1A signal, except when the
    #    right delay is masked.
    # 2. Compute the ratio of clusters masked by the cluster masking algorithm
    #    for each delay. Due to statistical fluctuations or inefficiencies, the
    #    sum of all delays may or may not be 1.
    # 3. Identify the delays which bring the largest cluster rate drop until
    #    the total cluster rate increase left drops below the cut ratio.
    list_condition = []
    for (fed, slot, oh), oh_df in df.groupby(["fed", "slot", "oh"]):
        oh_df = oh_df.copy()

        # Step 1
        baseline = int(oh_df.loc[oh_df["delay"] == -2, "cluster-rate-unmasked"])
        oh_df = oh_df[oh_df["delay"] >= 0]
        oh_df["cluster-rate-unmasked"] -= baseline / oh_df["l1a-period"]

        # Step 2
        oh_df["rate-drop"] = (
            oh_df["cluster-rate-unmasked"] - oh_df["cluster-rate-masked"]
        ) / oh_df["cluster-rate-unmasked"]

        # Step 3
        oh_df = oh_df.sort_values(["rate-drop"])
        oh_df["total-rate-drop"] = oh_df["rate-drop"].cumsum()
        delay = oh_df.loc[oh_df["total-rate-drop"] > cut, "delay"].to_list()

        if len(delay):
            list_condition.append(
                {
                    "fed": fed,
                    "slot": slot,
                    "oh": oh,
                    "delay": delay,
                }
            )

    df_condition = pd.DataFrame.from_records(list_condition)
    df_condition = df_condition.explode("delay").astype(int).reset_index()

    # Save output file of analysis with delays that satisfy the condition
    os.makedirs(output_directory, exist_ok=True)
    output_filename = output_directory / "cluster-values.dat"
    df_condition.to_csv(output_filename, sep=";", index=False)

    # Save cluster rate plots if desired
    if plotting:
        os.makedirs(output_directory / "plots", exist_ok=True)

        pool = Pool(cpu_count())

        for (fed, slot, oh), df_oh in df.groupby(["fed", "slot", "oh"]):
            df_oh_condition = df_condition[
                (df_condition["fed"] == fed)
                & (df_condition["slot"] == slot)
                & (df_condition["oh"] == oh)
            ]

            pool.apply_async(
                _plot_scan_oh,
                args=(output_directory / "plots", df_oh, df_oh_condition),
                error_callback=lambda e: print(
                    "Error: " + repr(e)  # Just display the error for now
                ),
            )

        pool.close()
        pool.join()

    return output_filename
