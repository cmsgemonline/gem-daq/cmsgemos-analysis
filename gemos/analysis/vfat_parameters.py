"""VFAT configuration file routines"""

import os, re
from pathlib import Path
from shutil import copytree

import pandas as pd
import uproot

from gemos.analysis.vfat_calibrations import add_parameters


PARAMETER_MAPPING = {
    "iref": "CFG_IREF",
    "threshold": "CFG_THR_ARM_DAC",
    "zcc-threshold": "CFG_THR_ZCC_DAC",
    "vref-adc": "CFG_VREF_ADC",
    "latency": "CFG_LATENCY",
}


def _ffs(a):
    """
    Find the first set bit in an integer.

    See the ffs*() functions family in the POSIX libc.
    """
    return (a & -a).bit_length() - 1


def _bit_replace(data, value, mask=None):
    """
    Perform bits replacement in an integer.

    :param data: Original integer
    :param value: Value to inject at the place of the mask
    :param mask: Mask where to replace the bits
    :return: Original integer with value substituted in place of mask
    """
    if pd.isna(mask):
        return value
    else:
        mask = int(mask)
        data = data & ~mask
        value = (value << _ffs(mask)) & mask
        return data | value


def create_configuration(
    input_directory,
    output_directory,
    default_filename,
    mapping_filename,
    calibration_filename,
    dac_filenames,
    threshold_filenames,
    zcc_threshold_filenames,
    threshold_bump,
    hyst_factor,
    latency_filenames,
    latency_bump,
    register_set_filenames,
    scurve_filenames,
    mask_noisy_scurve=True,
    mask_empty_scurve=True,
):
    def write_configuration(df):
        """Write the configuration file for a given VFAT"""

        # This function is meant to be called by GroupBy.apply() but modifies
        # in-place the input DataFrame to keep track of the applied parameters.
        # Modifying in-place the input DataFrame is not recommended and, in
        # practice, causes crashes. Copy the DataFrame instead.
        df = df.copy()

        fed = df.iloc[0]["fed"].astype(int)
        slot = df.iloc[0]["slot"].astype(int)
        oh = df.iloc[0]["oh"].astype(int)
        vfat = df.iloc[0]["vfat"].astype(int)

        input_cfg_path = "{}/fed{}-slot{}/config-oh{}-vfat{}.cfg".format(
            input_directory, fed, slot, oh, vfat
        )
        output_cfg_path = "{}/fed{}-slot{}/config-oh{}-vfat{}.cfg".format(
            output_directory, fed, slot, oh, vfat
        )

        # Fall back to the default configuration file if no specific input file
        if not os.path.isfile(input_cfg_path):
            if default_filename and os.path.isfile(default_filename):
                print(
                    "Using default input configuration file for ({}, {}, {}, {})".format(
                        fed, slot, oh, vfat
                    )
                )
                input_cfg_path = default_filename
            else:
                print(
                    "Warning: no input file for ({}, {}, {}, {}), skipping VFAT...".format(
                        fed, slot, oh, vfat
                    )
                )
                return

        # Read VFAT configuration file
        with open(input_cfg_path, "r") as input_cfg_file:
            input_cfg_lines = input_cfg_file.readlines()

        output_cfg_lines = list()

        # Write lines contained in DataFrame to VFAT cfg file
        for line in input_cfg_lines:
            # Skip empty lines and comments:
            if line[0] == "\n" or line[0] == "#":
                pass
            else:
                config_pair = line.split(" ")
                config_parameter = config_pair[0]
                config_value = int(config_pair[1])

                # Filter DataFrame to get values for current parameter:
                df_parameter = df[df["parameter"] == config_parameter]

                # Warn user if there are multiple config instances for same parameters on same VFAT
                duplicates = df_parameter.duplicated(keep="last")
                if duplicates.any():
                    print(
                        "Warning: duplicate parameter {} for fed {}, slot {}, oh {}, vfat {}. Using last specified values".format(
                            config_parameter, fed, slot, oh, vfat
                        )
                    )

                # If no rows in DataFrame, leave line as in default config file
                for index, row in df_parameter.iterrows():
                    if not duplicates[index]:
                        # Update config file line with last parameter value in DataFrame
                        config_value = _bit_replace(config_value, row["value"], row["mask"])

                    # Remove used parameters
                    df.drop(index, inplace=True)

                line = "{} {}\n".format(config_parameter, config_value)

            output_cfg_lines.append(line)

        # Append new configuration parameters
        df.drop_duplicates("parameter", keep="last", inplace=True)
        for _, row in df.iterrows():
            output_cfg_lines.append(
                "{} {}\n".format(row["parameter"], _bit_replace(0, row["value"], row["mask"]))
            )

        # Write the configuration file
        os.makedirs(os.path.dirname(output_cfg_path), exist_ok=True)
        with open(output_cfg_path, "w") as output_cfg_file:
            output_cfg_file.writelines(output_cfg_lines)
        return output_cfg_path

    # Warnings
    # FIXME: decide whether this is an error or not
    if (mapping_filename and not calibration_filename) or (
        not mapping_filename and calibration_filename
    ):
        print("Warning: VFAT mapping and calibration files must both be provided")

    # Sanitize input
    if output_directory:
        copytree(input_directory, output_directory, dirs_exist_ok=True)
    else:
        output_directory = input_directory

    # Parse the parameters files
    parameter_dataframes = list()  # Append here all DataFrames to be later concatenated

    if mapping_filename and calibration_filename:
        # Load input files
        df_mapping = pd.read_csv(mapping_filename, sep=";")
        df_calibration = pd.read_csv(calibration_filename, sep=";")
        df_mapping = add_parameters(df_mapping, df_calibration)

        # Keep only needed parameters
        df_mapping = df_mapping[["fed", "slot", "oh", "vfat", "vref-adc", "iref"]]

        # Rename parameter names using mapping to their register names
        df_mapping.rename(columns=PARAMETER_MAPPING, inplace=True)
        df_mapping = df_mapping.melt(
            id_vars=["fed", "slot", "oh", "vfat"], var_name="parameter", value_name="value"
        )
        parameter_dataframes.append(df_mapping)

    if dac_filenames:
        # Load input files
        df_dac = pd.concat((pd.read_csv(f, sep=";") for f in dac_filenames), ignore_index=True)

        # Keep only needed parameters
        df_dac = df_dac[["fed", "slot", "oh", "vfat", "dac-name", "dac-value"]]

        # This DataFrame is already well-formatted, we just need to rename the dac name columns
        df_dac.rename(columns={"dac-name": "parameter", "dac-value": "value"}, inplace=True)
        parameter_dataframes.append(df_dac)

    if threshold_filenames:
        # Load input files
        df_thresholds = pd.concat(
            (pd.read_csv(f, sep=";") for f in threshold_filenames), ignore_index=True
        )

        # Keep only needed parameters
        df_thresholds = df_thresholds[["fed", "slot", "oh", "vfat", "threshold"]]

        # Rename parameter names using mapping to their register names
        df_thresholds.rename(columns=PARAMETER_MAPPING, inplace=True)
        df_thresholds = df_thresholds.melt(
            id_vars=["fed", "slot", "oh", "vfat"], var_name="parameter", value_name="value"
        )
        df_thresholds["value"] += threshold_bump
        df_thresholds["value"].clip(upper=255, inplace=True)
        parameter_dataframes.append(df_thresholds)

        if hyst_factor:
            df_hyst = df_thresholds.copy()
            df_hyst["parameter"] = "CFG_HYST"
            df_hyst["value"] *= hyst_factor
            df_hyst["value"].clip(upper=63, inplace=True)
            df_hyst["value"] = df_hyst["value"].round(0).astype(int)
            parameter_dataframes.append(df_hyst)

    if zcc_threshold_filenames:
        # Load input files
        df_zcc_thresholds = pd.concat(
            (pd.read_csv(f, sep=";") for f in zcc_threshold_filenames), ignore_index=True
        )

        # Keep only needed parameters
        df_zcc_thresholds = df_zcc_thresholds[["fed", "slot", "oh", "vfat", "threshold"]].rename(
            columns={"threshold": "zcc-threshold"}
        )

        # Rename parameter names using mapping to their register names
        df_zcc_thresholds.rename(columns=PARAMETER_MAPPING, inplace=True)
        df_zcc_thresholds = df_zcc_thresholds.melt(
            id_vars=["fed", "slot", "oh", "vfat"], var_name="parameter", value_name="value"
        )
        df_zcc_thresholds["value"] += threshold_bump
        df_zcc_thresholds["value"].clip(upper=255, inplace=True)
        parameter_dataframes.append(df_zcc_thresholds)

    if latency_filenames:
        # Load input files
        df_latencies = pd.concat(
            (pd.read_csv(f, sep=";") for f in latency_filenames), ignore_index=True
        )

        # Keep only needed parameters
        df_latencies = df_latencies[["fed", "slot", "oh", "vfat", "latency"]]
        df_latencies["latency"] += latency_bump

        # Rename parameter names using mapping to their register names
        df_latencies.rename(columns=PARAMETER_MAPPING, inplace=True)
        df_latencies = df_latencies.melt(
            id_vars=["fed", "slot", "oh", "vfat"], var_name="parameter", value_name="value"
        )
        parameter_dataframes.append(df_latencies)

    if register_set_filenames:
        # Load input files
        df_register_sets = pd.concat(
            (pd.read_csv(f, sep=";") for f in register_set_filenames), ignore_index=True
        )

        # Add them unprocessed
        parameter_dataframes.append(df_register_sets)

    if scurve_filenames:
        # Load input files
        df_scurves = pd.concat(
            (uproot.open(f)["scurve-results"].arrays(library="pd") for f in scurve_filenames),
            ignore_index=True,
        )

        # Generate VFAT parameter name
        df_scurves["parameter"] = [
            "VFAT_CHANNELS.CHANNEL{}".format(int(row["channel"]))
            for _, row in df_scurves.iterrows()
        ]
        df_scurves["mask"] = 0x4000

        # Mask noisy and/or empty (i.e. dead) channels
        df_scurves["value"] = 0
        if mask_noisy_scurve:
            df_scurves.loc[df_scurves["noisy"] == 1, "value"] = 1
        if mask_empty_scurve:
            df_scurves.loc[df_scurves["empty"] == 1, "value"] = 1

        # Keep only needed parameters
        df_scurves = df_scurves[["fed", "slot", "oh", "vfat", "parameter", "value", "mask"]]

        parameter_dataframes.append(df_scurves)

    df_vfat_parameters = pd.concat(parameter_dataframes, ignore_index=True)
    df_vfat_parameters["mask"] = df_vfat_parameters.get("mask", None)

    # Update the configuration files for each VFAT present in the parameters DataFrame
    groups = df_vfat_parameters.groupby(["fed", "slot", "oh", "vfat"])
    output_filenames = groups.apply(write_configuration)

    print("VFAT configuration files written in {}".format(output_directory))
    return output_filenames


def dump_configuration(input_directory, output_filename):
    VFAT_COMPONENT_REGEX = re.compile("^fed(\d+)-slot(\d+)/config-oh(\d+)-vfat(\d+)$")

    output_dfs = list()

    for dirpath, _, filenames in os.walk(input_directory, followlinks=True):
        for filename in filenames:
            cfg = Path(os.path.join(dirpath, filename))
            if cfg.suffix != ".cfg":
                continue  # Not a configuration file

            component = cfg.relative_to(input_directory).with_suffix("").as_posix()
            vfat_location = re.match(VFAT_COMPONENT_REGEX, component)
            if vfat_location is None:
                continue  # Not a VFAT

            df = pd.read_csv(cfg, sep=" ", comment="#", names=["parameter", "value"])
            df["fed"] = int(vfat_location[1])
            df["slot"] = int(vfat_location[2])
            df["oh"] = int(vfat_location[3])
            df["vfat"] = int(vfat_location[4])
            output_dfs.append(df)

    output_df = pd.concat(output_dfs, ignore_index=True)
    output_df.to_csv(output_filename, sep=";", index=False)

    return output_filename
