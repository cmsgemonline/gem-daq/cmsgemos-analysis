"""Occupancy analysis routines"""

from multiprocessing import cpu_count, Pool
import gc
import os
import pathlib
import re

import matplotlib.pyplot as plt
import mplhep
import numpy as np
import pandas as pd
import uproot


plt.style.use(mplhep.style.CMS)


def _analyze_vfat(hist_chan_hit):
    """Identifies the noisy channels on a given VFAT and removes them from the occupancy histogram

    Note: The input histogram is both returned by the function *and modified in-place*.
    """

    # Remove first the channels with 0 hits
    array_chan_nonzero_hit = hist_chan_hit.values()
    array_chan_nonzero_hit = array_chan_nonzero_hit[array_chan_nonzero_hit != 0]

    noisy_channels = None
    # If VFAT has hits, find the noisy channels (defined as the outliers of the distribution) and clean up their contribution
    if len(array_chan_nonzero_hit) > 0:
        median = np.median(array_chan_nonzero_hit)
        q1, q3 = np.percentile(array_chan_nonzero_hit, [25, 75])

        # List the channels that are outliers, i.e. too noisy
        noisy_channels = np.nonzero(hist_chan_hit.view() > q3 + 1.5 * (q3 - q1))[0]

        # Produce a cleaned histogram to check that the cleaning worked
        hist_chan_hit.view()[hist_chan_hit.view() > q3 + 1.5 * (q3 - q1)] = 0

    return noisy_channels, hist_chan_hit


def _plot_scan_oh(output_directory, fed, slot, oh, result_oh):

    max_vfat = max(result_vfat[0] for result_vfat in result_oh)

    nrows, ncols = 3, int(np.ceil(max_vfat / 3))  # Round-up the number of columns
    scan_fig, scan_axs = plt.subplots(nrows, ncols, figsize=(ncols * 10, nrows * 10))
    scan_axs = scan_axs.flat

    for vfat, hist, hist_clean in result_oh:
        # Raw data
        color = "orange"
        scan_axs[vfat].set_ylabel("Raw hits", color=color)
        hist.plot(ax=scan_axs[vfat], color=color, yerr=False)
        scan_axs[vfat].tick_params(axis="y", labelcolor=color)
        if scan_axs[vfat].get_ylim()[0] < 0.1:
            scan_axs[vfat].set_ylim(0.1, (scan_axs[vfat].get_ylim())[1] * 10)
        scan_axs[vfat].set_yscale("log")

        # Cleaned data
        color = "blue"
        scan_axs_clean = scan_axs[vfat].twinx()
        scan_axs_clean.set_ylabel("Clean hits", color=color)
        hist_clean.plot(ax=scan_axs_clean, color=color, yerr=False)
        scan_axs_clean.tick_params(axis="y", labelcolor=color)
        if scan_axs_clean.get_ylim()[0] < 0.1:
            scan_axs_clean.set_ylim(0.1, (scan_axs_clean.get_ylim())[1] * 10)
        scan_axs_clean.set_yscale("log")

        scan_axs[vfat].set_xlim(0, 128)
        scan_axs[vfat].title.set_text(f"VFAT {vfat}")

    plt.tight_layout()

    output_figure = "{}/fed{}-slot{}-oh{}.png".format(output_directory, fed, slot, oh)
    scan_fig.savefig(output_figure)
    print("Scan plot saved to {}".format(output_figure))

    # Make sure to keep the memory usage reasonable
    plt.close(scan_fig)
    gc.collect()


def analyze(input_filenames, output_directory, plotting=False, iteration_max=0):

    # Output dataframe noisy channels
    output_dataframe_noisy_channels = pd.DataFrame(
        columns=["fed", "slot", "oh", "vfat", "noisy-channel", "iteration"]
    )

    # Load the input files
    for filename in input_filenames:
        print("Working on file:", filename)
        file = uproot.open(pathlib.Path(filename))

        result_oh = {}
        for dir_name, directory in file.iteritems(recursive=False):
            # Loop over directories in root file
            dir_name = dir_name.split(";")[0]
            fed, slot, oh, vfat = re.findall(r"\d+", dir_name)

            hist_chan_hit = directory["nHitvsChan"].to_hist()

            # Make a copy of the hist so it can be cleaned iteratively by the routine _analyze_vfat
            hist_chan_hit_clean = hist_chan_hit.copy()

            noise = True
            iteration = 0
            while noise is True and ((iteration < iteration_max) if iteration_max != 0 else True):
                noisy_channels, hist_chan_hit_clean = _analyze_vfat(hist_chan_hit_clean)

                if noisy_channels is not None:
                    for chan in noisy_channels:
                        output_dataframe_noisy_channels.loc[
                            len(output_dataframe_noisy_channels.index)
                        ] = [fed, slot, oh, vfat, chan, iteration]
                    iteration += 1
                    if len(noisy_channels) < 1:
                        noise = False
                else:
                    noise = False

            result_oh.setdefault((fed, slot, oh), []).append(
                [int(vfat), hist_chan_hit, hist_chan_hit_clean]
            )

        if plotting:
            os.makedirs(output_directory / "plots", exist_ok=True)
            pool = Pool(cpu_count())

            for (fed, slot, oh), value in result_oh.items():
                pool.apply_async(
                    _plot_scan_oh,
                    args=(output_directory / "plots", fed, slot, oh, value),
                    error_callback=lambda e: print(
                        "Error: " + repr(e)  # Just display the error for now
                    ),
                )

            pool.close()
            pool.join()

    # Save the list of noisy channels
    os.makedirs(output_directory, exist_ok=True)
    output_filename_noisy_channels = output_directory / "noisy-channels.dat"
    output_dataframe_noisy_channels.to_csv(output_filename_noisy_channels, sep=";", index=False)

    return output_filename_noisy_channels
