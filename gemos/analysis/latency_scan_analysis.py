"""Latency scan analysis routines"""

import gc
import os
import pathlib

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import mplhep
import re
import uproot
from multiprocessing import cpu_count, Pool

plt.style.use(mplhep.style.CMS)


def _analyze_vfat(hist_events, hist_one_hit, hist_chan_hit):
    """
    This function analyzes the latency scan histograms for a single VFAT. The
    histograms are normalized to the number of event in place and the best
    latency returned. At the moment the latency value is chosen as the one at
    the peak maximum. This algorithm is correct only for a pulse length of 1.
    """
    # Set latency bins with 0 entries to 1 to avoid problems when normalizing
    hist_events_nonzero = hist_events.view().copy()
    hist_events_nonzero[hist_events_nonzero == 0] = 1

    # Normalize hit histograms according to the number of events in each latency bin
    hist_chan_hit_view = hist_chan_hit.view()
    for i in range(0, 128):
        hist_chan_hit_view[:, i] /= hist_events_nonzero

    hist_one_hit /= hist_events_nonzero

    # If VFAT has hits compute the latency
    latency = None
    if hist_one_hit.values().any() > 0:
        latency = np.argmax(hist_one_hit.values())

    return latency


def _plot_scan_vfat(
    output_directory,
    fed,
    slot,
    oh,
    vfat,
    latency,
    hist_one_hit,
    hist_chan_hit,
    b_min,
    b_max,
):
    """
    Produces per-VFAT plots with:
    * the normalized number of hits/events per channel;
    * the number of hits as function of the channel and latency point;
    * the fraction of events with at least one hit registered for each latency
      value.
    """
    nrows, ncols = 1, 3
    scan_fig, scan_axs = plt.subplots(nrows, ncols, figsize=(ncols * 10, nrows * 10))
    scan_axs = scan_axs.flat

    # Per channel occupancy plot
    hist_chan_hit.project(1).plot(ax=scan_axs[0], color="violet", lw=2, ls="-.", yerr=False)
    scan_axs[0].set_xlabel("Channel number")
    scan_axs[0].set_ylabel("Fraction of events with hits")

    # Per channel latency plot
    hist_chan_hit.plot2d(ax=scan_axs[1])
    scan_axs[1].set_xlim(b_min, b_max)

    # Per VFAT latency plots (one hit vs number hits)
    scan_axs[2].set_xlim(b_min, b_max)
    scan_axs[2].text(
        0.8,
        0.5,
        "Latency={}".format(latency),
        horizontalalignment="center",
        verticalalignment="center",
        transform=scan_axs[2].transAxes,
        color="blue",
    )
    scan_axs[2].axvline(
        x=latency,
        color="blue",
        lw=3,
    )
    hist_chan_hit.project(0).plot(ax=scan_axs[2], color="lightgreen", lw=2, ls="-.", yerr=False)
    scan_axs[2].set_ylabel("Ratio number of hits/number of events")
    scan_axs_one_hit = scan_axs[2].twinx()
    hist_one_hit.plot(ax=scan_axs_one_hit, color="blue", lw=2, ls="-.", yerr=False)
    scan_axs_one_hit.set_ylabel("Fraction of events with at least 1 hit")

    # Save figure
    output_figure = "{}/fed{}-slot{}-oh{}-vfat{}.png".format(output_directory, fed, slot, oh, vfat)
    scan_fig.savefig(output_figure)
    print("Scan plot saved to {}".format(output_figure))

    # Keep memory usage as reasonable as possible
    plt.close(scan_fig)
    gc.collect()


def _plot_scan_oh(output_directory, fed, slot, oh, result_oh):
    """
    Plots the latency scan results avaraged over the VFATs of  a given OH.
    """
    nrows, ncols = 1, 1
    scan_fig, scan_axs = plt.subplots(nrows, ncols, figsize=(ncols * 10, nrows * 10))

    # Create an hist taking the copy of the first one
    hist = result_oh[0][1].copy()

    # Initialize the hist with 0
    hist_view = hist.view()
    hist_view = 0

    # Loop over the VFAT in OH and sum them
    vfat_count = 0
    for pair_res in result_oh:
        hist += pair_res[1]
        vfat_count += 1

    # Normalize per VFAT
    hist_view /= vfat_count

    # Plot!
    hist.plot(ax=scan_axs, color="lightgreen", lw=2, ls="-.", yerr=False)

    min_b = min(list(b_min for i, h, b_min, b_max in result_oh))
    max_b = max(list(b_max for i, h, b_min, b_max in result_oh))

    scan_axs.set_xlim(min_b, max_b)
    scan_axs.set_ylabel("Fraction of events with at least 1 hit averaged over VFATs")

    # Save figure
    output_figure = "{}/fed{}-slot{}-oh{}.png".format(output_directory, fed, slot, oh)
    scan_fig.savefig(output_figure)
    print("Scan plot saved to {}".format(output_figure))

    # Keep memory usage as reasonable as possible
    plt.close(scan_fig)
    gc.collect()


def analyze(input_filenames, output_directory, plotting=False):
    # Best latency output dataframe
    output_dataframe = pd.DataFrame(columns=["fed", "slot", "oh", "vfat", "latency"])

    # Load the input files
    for filename in input_filenames:
        print("Processing file:", filename)
        file = uproot.open(pathlib.Path(filename))

        if plotting:
            os.makedirs(output_directory / "plots", exist_ok=True)
            pool = Pool(cpu_count())

        # Loop over directories (i.e. VFAT) in the ROOT file
        result_oh = {}
        for dir_name, directory in file.iteritems(recursive=False, cycle=False):
            fed, slot, oh, vfat = re.findall(r"\d+", dir_name)

            hist_events = directory["nEventvsLatency"].to_hist()
            hist_one_hit = directory["oneHitvsLatency"].to_hist()
            hist_chan_hit = directory["chanHitvsLatency2D"].to_hist()

            # Define the margins, bin min (b_min) and bin max (b_max), as 10
            # bins away from the beginning and the end of the scanned latency
            # range
            latency_range = np.nonzero(hist_events.view())
            b_min = latency_range[0][0] - 10
            b_max = latency_range[0][-1] + 10

            latency = _analyze_vfat(hist_events, hist_one_hit, hist_chan_hit)

            result_oh.setdefault((fed, slot, oh), []).append(
                [int(vfat), hist_one_hit, b_min, b_max]
            )

            # Save latency in dataframe if valid, i.e. VFAT had hits
            if latency is not None:
                output_dataframe.loc[len(output_dataframe.index)] = [fed, slot, oh, vfat, latency]

            # Plot per VFAT
            if plotting:
                pool.apply_async(
                    _plot_scan_vfat,
                    args=(
                        output_directory / "plots",
                        fed,
                        slot,
                        oh,
                        vfat,
                        latency,
                        hist_one_hit,
                        hist_chan_hit,
                        b_min,
                        b_max,
                    ),
                    error_callback=lambda e: print(
                        "Error VFAT plots: fed {}, slot {}, oh {}, vfat {} ".format(
                            fed, slot, oh, vfat
                        )
                        + repr(e)  # Just display the error for now
                    ),
                )

        # Plot per OH
        if plotting:
            for fed, slot, oh in result_oh:
                pool.apply_async(
                    _plot_scan_oh,
                    args=(output_directory / "plots", fed, slot, oh, result_oh[fed, slot, oh]),
                    error_callback=lambda e: print(
                        "Error OH plots: fed {}, slot {}, oh {} ".format(fed, slot, oh)
                        + repr(e)  # Just display the error for now
                    ),
                )

            pool.close()
            pool.join()

    # Save output file of analysis with delays that satisfy the condition
    os.makedirs(output_directory, exist_ok=True)
    output_filename = output_directory / "latency-values.dat"
    output_dataframe.to_csv(output_filename, sep=";", index=False)

    return output_filename
