"""GBT phase scan routines"""

import os

import pandas as pd


def create_configuration(input_filenames, output_directory):
    def write_configuration(df):
        """Write the configuration file for a given OptoHybrid"""
        fed = df.iloc[0]["fed"]
        slot = df.iloc[0]["slot"]
        oh = df.iloc[0]["oh"]

        output_filename = "{}/fed{}-slot{}/config-oh{}-gbt.cfg".format(
            output_directory, fed, slot, oh
        )
        os.makedirs(os.path.dirname(output_filename), exist_ok=True)

        df.to_csv(
            output_filename, sep=" ", index=False, header=False, columns=["config-name", "phase"]
        )
        return output_filename

    df = pd.concat((pd.read_csv(f, sep=";") for f in input_filenames), ignore_index=True)

    # Create a new column with the configuration name
    df["config-name"] = "PHASE_VFAT" + df["vfat"].astype(str)

    # Create one file per OptoHybrid
    groups = df.groupby(["fed", "slot", "oh"])
    output_filenames = groups.apply(write_configuration)

    print("GBT configuration files written in {}".format(output_directory))
    return output_filenames
