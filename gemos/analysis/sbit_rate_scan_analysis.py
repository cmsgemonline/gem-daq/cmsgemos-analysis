"""S-bit rate scan analysis routines"""

import os
import gc
import pandas as pd
from multiprocessing import Pool, cpu_count
import matplotlib.pyplot as plt
import mplhep
from numpy import ceil
from multiprocessing import Pool, cpu_count

plt.style.use(mplhep.style.CMS)


def plot_scan_oh(group, output_directory):
    """Plot threshold scans for all VFATs in single OH"""
    (fed, slot, oh), df = group

    # The S-bit counters increase of maximum 1 per each bunch crossing;
    # the bunch crossing rate in LHC is 40.07897 MHz approximated to 4008000 MHz
    # giving a possible increase in the s-bit counters of maximum 4008000 per second.
    # The time interval used for the scan can be estimated in second as:
    time_interval = (df["rate"].max() // 40080000) + 1

    nrows, ncols = 3, int(ceil(df["vfat"].max() / 3))  # Round-up the number of columns
    fig, axs = plt.subplots(nrows, ncols, figsize=(ncols * 10, nrows * 10))
    axs = axs.flat

    for vfat, vfat_df in df.groupby("vfat"):
        """Plot threshold scan for single VFAT"""
        axs[vfat].semilogy(vfat_df["dac-value"], vfat_df["rate"], "o")
        # The axis limit is chosen to be twice the maximum rate aquired in the time interval of the scan
        axs[vfat].set_ylim(1, time_interval * 40080000 * 2)
        axs[vfat].set_xlim(0, 255)
        axs[vfat].title.set_text(f"VFAT {vfat}")
        axs[vfat].set_xlabel("THR_ARM_DAC")
        axs[vfat].set_ylabel("S-bt rate (Hz)")

    plt.tight_layout()

    output_figure = "{}/plots/fed{}-slot{}-oh{}.png".format(output_directory, fed, slot, oh)
    fig.savefig(output_figure)
    print("Scan plot saved to {}".format(output_figure))

    # Make sure to keep the memory usage reasonable
    plt.close(fig)
    gc.collect()


def create_configuration(input_filenames, output_directory, target_rate, plotting=True):
    # Load the input files
    df = pd.concat((pd.read_csv(f, sep=";", dtype=int) for f in input_filenames), ignore_index=True)

    # Average the scan over the iterations
    df = (
        df.drop("iteration", axis=1)
        .groupby(["fed", "slot", "oh", "vfat", "dac-value"])
        .mean()
        .astype(int)
        .reset_index()
    )

    # Compute the threshold values
    condition = df["rate"].le(target_rate)

    def analyze_scan(df):
        """Helper function to get the threshold value for a given VFAT"""
        try:  # Set threshold as first value for which rate <= target_rate, if it exists
            threshold_arm_dac = df.loc[condition].iloc[0]
        except IndexError:  # Use the highest value scanned, if the condition is never met
            threshold_arm_dac = df.iloc[-1]

        return threshold_arm_dac

    df["threshold"] = df.groupby(["fed", "slot", "oh", "vfat"])["dac-value"].transform(analyze_scan)

    # Save thresholds
    os.makedirs(output_directory, exist_ok=True)
    output_filename = output_directory / "threshold-values.dat"

    df.drop_duplicates(["fed", "slot", "oh", "vfat"]).loc[
        :, ["fed", "slot", "oh", "vfat", "threshold"]
    ].to_csv(output_filename, sep=";", index=False)

    print("Output threshold file written to {}".format(output_filename))

    # Plot threshold scans for all VFATs in same OH in one canvas
    if plotting:
        os.makedirs(output_directory / "plots", exist_ok=True)

        pool = Pool(cpu_count())

        for group in df.groupby(["fed", "slot", "oh"]):
            pool.apply_async(
                plot_scan_oh,
                args=(
                    group,
                    output_directory,
                ),
                error_callback=lambda e: print(
                    "Error: " + repr(e)  # Just display the error for now
                ),
            )

        pool.close()
        pool.join()

    return output_filename
