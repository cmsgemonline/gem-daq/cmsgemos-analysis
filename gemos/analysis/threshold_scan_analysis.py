"""Threshold scan analysis routines"""

import os
import gc
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mplhep
from multiprocessing import cpu_count, Pool

plt.style.use(mplhep.style.CMS)


def filter_not_readout(input_df):
    """
    This function identifies and labels the not-readout VFAT.

    A VFAT is considered as not-readout if its channels never send hits.

    :param pandas.DataFrame input_df: Threshold input file.
    """
    sum = input_df.groupby(["fed", "slot", "oh", "vfat"])["hits"].transform("sum")
    input_df.loc[sum == 0, "state"] = "not-readout"


def filter_broken_channels(input_df):
    """
    This function identifies and labels broken channels.

    A channel is considered as broken if it has 0 hits for every threshold parameter.

    :param pandas.DataFrame input_df: Threshold input file.
    """
    sum = input_df.groupby(["fed", "slot", "oh", "vfat", "channel"])["hits"].transform("sum")
    input_df.loc[(input_df["state"] != "not-readout") & (sum == 0), "state"] = "broken"


def threshold_plot(oh_group, nrows, ncols, output_dir):
    """Performs the summary threshold plot for an OptoHybrid. It produces a figure with nrows*ncols
    2D plots of the numbers of hits per channel and threshold.

    :param pandas.DataFrameGroupBy oh_group: groupby object that contains the information of OH.
    :param int nrows: number of rows to organize the subplots.
    :param int ncols: number of cols to organize the subplots.
    :param pathlib.PosixPath output_dir: Path to save the images.
    """
    (fed, slot, oh), oh_df = oh_group

    fig, axs = plt.subplots(nrows, ncols, figsize=(55, 25))
    axs = axs.flat

    graph_name = output_dir / "plots/thr-summary-{}-{}-{}.png".format(fed, slot, oh)

    for vfat, df in oh_df.groupby("vfat"):
        # a map per chamber vfat is builded
        map = []
        for _, df in df.groupby("parameter"):
            map.append((df["hits"] / df["triggers"]).tolist())
        map = np.array(map)

        im = axs[vfat].imshow(map, origin="lower", aspect=128.0 / 30.0)
        axs[vfat].set_title("VFAT {}".format(vfat), fontsize=18)
        axs[vfat].set_xlabel("VFAT Channel", fontsize=13)
        axs[vfat].set_ylabel("THR_ARM_DAC [DAC units]", fontsize=13)
        axs[vfat].tick_params(axis="x", labelsize=10)
        axs[vfat].tick_params(axis="y", labelsize=10)
        fig.colorbar(im, ax=axs[vfat], pad=0.01, fraction=0.048)

        ins_bar = axs[vfat].inset_axes([0, 1.02, 1, 0.1])

        for kind, color in {"not-readout": "black", "broken": "red"}.items():
            [
                ins_bar.axvline(x_, linewidth=1, color=color, lw=2, label=kind)
                for x_ in df.loc[df["state"] == kind, "channel"].drop_duplicates()
            ]

        handles, labels = ins_bar.get_legend_handles_labels()
        labels, ids = np.unique(labels, return_index=True)
        handles = [handles[i] for i in ids]
        axs[vfat].legend(handles, labels, bbox_to_anchor=(0.5, -0.05))

        ins_bar.set_xlim(0, 128)
        ins_bar.set_xticks(range(0, 128, 20))
        ins_bar.set_xticklabels([])
        ins_bar.set_yticklabels([])

    plt.tight_layout()
    fig.savefig(graph_name)
    print("Threshold plot saved to ", graph_name, flush=True)

    # Make sure to keep the memory usage reasonable
    plt.close(fig)
    gc.collect()


def compute_ratios(df):
    ratios = {"fed": pd.DataFrame(), "oh": pd.DataFrame()}

    # -------------------------------------helper function------------------------------------------
    def compute_per(kind):
        print("For ", kind)

        group_by = {"fed": ["fed"], "oh": ["fed", "slot", "oh"]}[kind]

        for indexs, group in df.groupby(group_by):
            group = group.drop_duplicates(["fed", "slot", "oh", "vfat", "channel"])
            pcts = group["state"].value_counts(normalize=True).to_dict()

            try:
                info = {key: value for key, value in zip(group_by, indexs)}
            except:
                info = {key: value for key, value in zip(group_by, [indexs])}

            for key in ["fully-working", "not-readout", "broken"]:
                try:
                    info[key] = pcts[key]
                except:
                    info[key] = "--"

            ratios[kind] = ratios[kind].append(info, ignore_index=True)

    # ----------------------------------------------------------------------------------------------

    # Per FED
    compute_per("fed")
    # Per OH
    compute_per("oh")

    return ratios


def render_summary_report(df):
    """This function prints on console a summary report of the percentages of working, not-readout,
    and, broken channels.

    :param pandas.DataFrame df: dataFrame with the channels information.
    """
    ratios = compute_ratios(df)

    print(
        "=================================================================\n"
        + "   Percentages report of the working, not-readout and broken   \n"
        + 20 * " "
        + "channels for fed and oh\n"
        + "================================================================="
    )

    formatters = {
        "fed": {"fed": "{:n}".format},
        "oh": {"fed": "{:n}".format, "slot": "{:n}".format, "oh": "{:n}".format},
    }

    for key in ratios.keys():
        print(">> " + key + " " + (59 - len(key)) * "-" + "\n")
        print(ratios[key].to_string(index=False, justify="center", formatters=formatters[key]))

    print("=================================================================")


def create_configuration(
    input_filenames,
    output_dir,
    plotting=False,
):
    """Routine to perform the analysis"""
    os.makedirs(output_dir, exist_ok=True)

    # Load the input files
    input_df = pd.concat((pd.read_csv(f, sep=";") for f in input_filenames), ignore_index=True)
    # Channels with 0 triggers have not been scanned
    input_df = input_df[input_df["triggers"] != 0]

    # Channels initialized as healthy
    input_df["state"] = "fully-working"

    # Find problematic channels
    print("Filtering not-readout vfats...")
    filter_not_readout(input_df)
    print("Filtering broken channels...")
    filter_broken_channels(input_df)

    # Saving broken channels info
    filename = output_dir / "non-working-channels.dat"
    input_df.loc[input_df["state"] == "broken", "fed":"channel"].drop_duplicates().to_csv(
        filename, sep=";", header=True, index=False
    )
    print("Non-working channels info saved to ", filename)

    print("Computing percentage reports...")
    render_summary_report(input_df)

    # Perform plotting
    if plotting:
        print("Plotting...", flush=True)
        os.makedirs("{}/plots".format(output_dir), exist_ok=True)

        nrows, ncols = 3, int(np.ceil(len(input_df["vfat"].drop_duplicates()) / 3))

        pool = Pool(cpu_count())

        for oh_group in input_df.groupby(["fed", "slot", "oh"]):
            # Produce one image per OptoHybrid
            pool.apply_async(
                threshold_plot,
                args=(oh_group, nrows, ncols, output_dir),
                error_callback=lambda e: print(
                    "Error: " + repr(e)  # Just display the error for now
                ),
            )

        pool.close()
        pool.join()

    print("Done!")
