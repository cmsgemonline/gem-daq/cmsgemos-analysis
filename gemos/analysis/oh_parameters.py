"""OptoHybrid configuration file routines"""

import os
from shutil import copytree

import pandas as pd


def create_configuration(
    input_directory,
    output_directory,
    cluster_mask_filenames,
    sbit_delay_filenames,
):
    def write_configuration(df):
        """Write the configuration file for a given OptoHybrid"""

        # This function is meant to be called by GroupBy.apply() but modifies
        # in-place the input DataFrame to keep track of the applied parameters.
        # Modifying in-place the input DataFrame is not recommended and, in
        # practice, causes crashes. Copy the DataFrame instead.
        df = df.copy()

        fed = df.iloc[0]["fed"]
        slot = df.iloc[0]["slot"]
        oh = df.iloc[0]["oh"]

        input_cfg_path = "{}/fed{}-slot{}/config-oh{}.cfg".format(input_directory, fed, slot, oh)
        output_cfg_path = "{}/fed{}-slot{}/config-oh{}.cfg".format(output_directory, fed, slot, oh)

        # Read OH configuration file
        if os.path.isfile(input_cfg_path):
            with open(input_cfg_path, "r") as input_cfg_file:
                input_cfg_lines = input_cfg_file.readlines()
        else:
            print("Using empty default input configuration for ({}, {}, {})".format(fed, slot, oh))
            input_cfg_lines = list()

        output_cfg_lines = list()

        # Write lines contained in DataFrame to OH cfg file
        for line in input_cfg_lines:
            # Skip empty lines and comments:
            if line[0] == "\n" or line[0] == "#":
                pass
            else:

                config_pair = line.split(" ")
                config_parameter = config_pair[0]

                # Filter DataFrame to get values for current parameter:
                df_parameter = df[(df["parameter"] == config_parameter)]

                n_parameters = len(df_parameter)
                # If no rows in DataFrame, leave line as in default config file
                if n_parameters > 0:
                    if n_parameters > 1:
                        # Warn user if there are multiple config instances for same parameters on same OH
                        print(
                            "Warning: duplicate parameter {} for fed {}, slot {}, oh{}. Using last specified values".format(
                                config_parameter, fed, slot, oh
                            )
                        )

                    # Update config file line with last parameter value in DataFrame:
                    config_pair[1] = str(df_parameter.iloc[-1]["value"]) + "\n"
                    line = " ".join(config_pair)

                    # Remove used parameters
                    df.drop(df_parameter.index, inplace=True)

            output_cfg_lines.append(line)

        # Append new configuration parameters
        df.drop_duplicates("parameter", keep="last", inplace=True)
        for _, row in df.iterrows():
            output_cfg_lines.append("{} {}\n".format(row["parameter"], row["value"]))

        # Write the configuration file
        os.makedirs(os.path.dirname(output_cfg_path), exist_ok=True)
        with open(output_cfg_path, "w") as output_cfg_file:
            output_cfg_file.writelines(output_cfg_lines)
        return output_cfg_path

    # Sanitize input
    if output_directory:
        copytree(input_directory, output_directory, dirs_exist_ok=True)
    else:
        output_directory = input_directory

    # Parse the parameters files
    parameter_dataframes = list()  # Append here all DataFrames to be later concatenated

    if cluster_mask_filenames:
        df_cluster = pd.concat(
            (pd.read_csv(f, sep=";") for f in cluster_mask_filenames), ignore_index=True
        )

        # Keep only needed parameters
        df_cluster = df_cluster[["fed", "slot", "oh", "delay"]]

        # Compute the value for the register given the delays that need to be masked
        df_mask = pd.DataFrame(columns=["fed", "slot", "oh", "parameter", "value"])
        for (fed, slot, oh), df in df_cluster.groupby(["fed", "slot", "oh"]):
            mask = 0
            for delay in df["delay"].values:
                mask += 2 ** delay
            df_mask.loc[len(df_mask.index)] = [
                fed,
                slot,
                oh,
                "TRIG.L1A_MASK.L1A_MASK_BITMASK",
                mask,
            ]

        parameter_dataframes.append(df_mask)

    if sbit_delay_filenames:
        df_sbit_delay = pd.concat(
            (pd.read_csv(f, sep=";") for f in sbit_delay_filenames), ignore_index=True
        )

        df_sbit_delay.rename(columns={"delay": "value"}, inplace=True)

        # Whether the S-bit delay should be enabled or not
        df_sbit_delay_en = df_sbit_delay.copy()
        df_sbit_delay_en["parameter"] = [
            "TRIG.SBIT_BX_DELAY_EN.VFAT{}_GROUP{}".format(row["vfat"], row["group"])
            for _, row in df_sbit_delay_en.iterrows()
        ]
        df_sbit_delay_en.loc[df_sbit_delay_en["value"] != 0, "value"] = 1
        df_sbit_delay_en.loc[df_sbit_delay_en["value"] == 0, "value"] = 0

        # The S-bit delay in itself
        # Note the offset of 1 BX when the feature is enabled
        df_sbit_delay["parameter"] = [
            "TRIG.SBIT_BX_DELAY.VFAT{}_GROUP{}".format(row["vfat"], row["group"])
            for _, row in df_sbit_delay.iterrows()
        ]
        df_sbit_delay.loc[df_sbit_delay["value"] != 0, "value"] -= 1

        # We need both the "value" and "enable" registers
        df = pd.concat((df_sbit_delay, df_sbit_delay_en), ignore_index=True)

        parameter_dataframes.append(df)

    df_oh_parameters = pd.concat(parameter_dataframes, ignore_index=True)

    # Update the configuration files for each OH present in the parameters DataFrame
    groups = df_oh_parameters.groupby(["fed", "slot", "oh"])
    output_filenames = groups.apply(write_configuration)

    print("OptoHybrid configuration files written in {}".format(output_directory))
    return output_filenames
