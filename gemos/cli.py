"""Main CLI interface"""

import argparse
import pathlib

from gemos.analysis import cluster_scan_analysis
from gemos.analysis import dac_scan_analysis
from gemos.analysis import gbt_phase_scan
from gemos.analysis import latency_scan_analysis
from gemos.analysis import occupancy_analysis
from gemos.analysis import oh_parameters
from gemos.analysis import sbit_rate_scan_analysis
from gemos.analysis import scurve_analysis
from gemos.analysis import threshold_scan_analysis
from gemos.analysis import vfat_calibrations
from gemos.analysis import vfat_parameters


def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(required=True, dest="command")

    # "analyze" command
    analyze_parser = subparsers.add_parser("analyze")
    analyze_subparsers = analyze_parser.add_subparsers(required=True, dest="subcommand")

    # "analyze cluster" subcommand
    analyze_cluster_parser = analyze_subparsers.add_parser("cluster")
    analyze_cluster_parser.add_argument(
        "-c",
        "--cut",
        type=float,
        default=0.05,
        help="Specify the fraction of high-multiplicity events accepted (w.r.t. the maximal cluster rate increase)",
    )
    analyze_cluster_parser.add_argument(
        "-p",
        "--plotting",
        action="store_true",
        help="Specify in case cluster mask rate plots are desired",
    )
    analyze_cluster_parser.add_argument(
        "inputfiles",
        type=pathlib.Path,
        nargs="+",
        help="Files containing the S-bit rate scan results",
    )
    analyze_cluster_parser.add_argument(
        "outputdir",
        type=pathlib.Path,
        help="Output directory in which to store the optimal cluster masks resulting from the analysis",
    )
    analyze_cluster_parser.set_defaults(
        func=lambda args: cluster_scan_analysis.analyze_scan(
            input_filenames=args.inputfiles,
            output_directory=args.outputdir,
            plotting=args.plotting,
            cut=args.cut,
        )
    )

    # "analyze dac" subcommand
    analyze_dac_parser = analyze_subparsers.add_parser("dac")
    analyze_dac_parser.add_argument(
        "-m",
        "--mapping",
        dest="mapping_file",
        type=pathlib.Path,
        help="File containing the VFAT chip ID to logical address mapping (see cmsgemos gem-dump-vfat-mapping)",
    )
    analyze_dac_parser.add_argument(
        "-c",
        "--calibration",
        dest="calibration_file",
        type=pathlib.Path,
        help="File containing the VFAT calibration parameters (see gemos query vfat-calibration)",
    )
    analyze_dac_parser.add_argument(
        "-p",
        "--plotting",
        type=dac_scan_analysis.PlottingCondition,
        default=dac_scan_analysis.PlottingCondition.WARNING,
        choices=list(dac_scan_analysis.PlottingCondition),
        help="Specify in case dac scan plots are desired",
    )
    analyze_dac_parser.add_argument(
        "inputfiles",
        type=pathlib.Path,
        nargs="+",
        help="Files containing the DAC scan results",
    )
    analyze_dac_parser.add_argument(
        "outputdir",
        type=pathlib.Path,
        help="Output directory in which to store the file (dac-values.dat) with the DAC scan parameters results of the analysis and optionally the plots",
    )
    analyze_dac_parser.set_defaults(
        func=lambda args: dac_scan_analysis.create_configuration(
            input_filenames=args.inputfiles,
            output_dir=args.outputdir,
            mapping_filename=args.mapping_file,
            calibration_filename=args.calibration_file,
            plotting=args.plotting,
        )
    )

    # "analyze latency" subcommand
    analyze_latency_parser = analyze_subparsers.add_parser("latency")
    analyze_latency_parser.add_argument(
        "-p",
        "--plotting",
        action="store_true",
        help="Specify in case latency plots are desired",
    )
    analyze_latency_parser.add_argument(
        "inputfiles",
        type=pathlib.Path,
        nargs="+",
        help="Files containing the latency scan bare histograms",
    )
    analyze_latency_parser.add_argument(
        "outputdir",
        type=pathlib.Path,
        help="Output directory in which to store the optimal latency values resulting from the analysis",
    )
    analyze_latency_parser.set_defaults(
        func=lambda args: latency_scan_analysis.analyze(
            input_filenames=args.inputfiles,
            output_directory=args.outputdir,
            plotting=args.plotting,
        )
    )

    # "analyze occupancy" subcommand
    analyze_occupancy_parser = analyze_subparsers.add_parser("occupancy")
    analyze_occupancy_parser.add_argument(
        "-p",
        "--plotting",
        action="store_true",
        help="Specify in case occupancy plots are desired",
    )
    analyze_occupancy_parser.add_argument(
        "-i",
        "--iteration_max",
        default=0,
        type=int,
        help="Specify the number of iterations desired (0 is unlimited)",
    )
    analyze_occupancy_parser.add_argument(
        "inputfiles",
        type=pathlib.Path,
        nargs="+",
        help="Files containing the occupancy data",
    )
    analyze_occupancy_parser.add_argument(
        "outputdir",
        type=pathlib.Path,
        help="Output directory in which to store the noisy channels from the analysis",
    )
    analyze_occupancy_parser.set_defaults(
        func=lambda args: occupancy_analysis.analyze(
            input_filenames=args.inputfiles,
            output_directory=args.outputdir,
            plotting=args.plotting,
            iteration_max=args.iteration_max,
        )
    )

    # "analyze sbit" subcommand
    analyze_sbit_parser = analyze_subparsers.add_parser("sbit")
    analyze_sbit_parser.add_argument(
        "-p",
        "--plotting",
        action="store_true",
        help="Specify in case S-bit rate plots are desired",
    )
    analyze_sbit_parser.add_argument(
        "-r",
        "--rate",
        default=100,
        type=int,
        help="Noise rate to be accepted at set threshold",
    )
    analyze_sbit_parser.add_argument(
        "inputfiles",
        type=pathlib.Path,
        nargs="+",
        help="Files containing the S-bit rate scan results",
    )
    analyze_sbit_parser.add_argument(
        "outputdir",
        type=pathlib.Path,
        help="Output directory in which to store the optimal thresholds resulting from the analysis",
    )
    analyze_sbit_parser.set_defaults(
        func=lambda args: sbit_rate_scan_analysis.create_configuration(
            input_filenames=args.inputfiles,
            output_directory=args.outputdir,
            target_rate=args.rate,
            plotting=args.plotting,
        )
    )

    # "analyze scurves"
    analyze_sc_parser = analyze_subparsers.add_parser("scurves")
    analyze_sc_parser.add_argument(
        "inputfiles",
        type=pathlib.Path,
        nargs="+",
        help="Files containing the S-curves histograms ROOT files",
    )
    analyze_sc_parser.add_argument(
        "outputdir",
        type=pathlib.Path,
        help="Output directory to save the results of the analysis",
    )
    analyze_sc_parser.add_argument(
        "-p",
        "--plotting",
        type=scurve_analysis.PlottingMode,
        default=scurve_analysis.PlottingMode.ESSENTIALS,
        choices=list(scurve_analysis.PlottingMode),
        help="Specify in case summary and/or boxplot S-curve plots are desired",
    )
    analyze_sc_parser.add_argument(
        "-f",
        "--full-output",
        dest="full_output",
        action="store_const",
        const=True,
        default=False,
        help="Specify when an output file that includes the input histograms be desired",
    )
    analyze_sc_parser.add_argument(
        "-n",
        "--noise-iqr",
        dest="noise_iqr",
        default=1.5,
        type=float,
        help="Interquartile range multiplicative factor used to identify noisy channels",
    )
    analyze_sc_parser.add_argument(
        "-u",
        "--units",
        type=scurve_analysis.Units,
        default=scurve_analysis.Units.DAC_UNITS,
        choices=list(scurve_analysis.Units),
        help="Specify in case fC or DAC units are desired",
    )
    analyze_sc_parser.add_argument(
        "-m",
        "--mapping",
        dest="mapping_file",
        type=pathlib.Path,
        help="File containing the VFAT chip ID to logical address mapping (see cmsgemos gem-dump-vfat-mapping)",
    )
    analyze_sc_parser.add_argument(
        "-c",
        "--calibration",
        dest="calibration_file",
        type=pathlib.Path,
        help="File containing the VFAT calibration parameters (see gemos query vfat-calibration)",
    )
    analyze_sc_parser.set_defaults(
        func=lambda args: scurve_analysis.create_configuration(
            input_filenames=args.inputfiles,
            output_dir=args.outputdir,
            mapping_filename=args.mapping_file,
            calibration_filename=args.calibration_file,
            plotting=args.plotting,
            units=args.units,
            full_output=args.full_output,
            noise_iqr=args.noise_iqr,
        )
    )

    # "analyze thresh"
    analyze_th_parser = analyze_subparsers.add_parser("thresh")
    analyze_th_parser.add_argument(
        "-p",
        "--plotting",
        action="store_true",
        help="Specify in case summary threshold plots are desired",
    )

    analyze_th_parser.add_argument(
        "inputfiles",
        type=pathlib.Path,
        nargs="+",
        help="Files containing the Threshold scan results",
    )
    analyze_th_parser.add_argument(
        "outputdir",
        type=pathlib.Path,
        help="Output directory to save the results of the analysis",
    )
    analyze_th_parser.set_defaults(
        func=lambda args: threshold_scan_analysis.create_configuration(
            input_filenames=args.inputfiles,
            output_dir=args.outputdir,
            plotting=args.plotting,
        )
    )

    # "create-config" command
    create_config_parser = subparsers.add_parser("create-config")
    create_config_subparsers = create_config_parser.add_subparsers(required=True, dest="subcommand")

    # "create-config gbt" subcommand
    create_config_gbt_parser = create_config_subparsers.add_parser("gbt")
    create_config_gbt_parser.add_argument(
        "inputfiles",
        type=pathlib.Path,
        nargs="+",
        help="Files containing the optimal GBT phases",
    )
    create_config_gbt_parser.add_argument(
        "outputdir",
        type=pathlib.Path,
        help="Output directory in which to store the GBT configuration files",
    )
    create_config_gbt_parser.set_defaults(
        func=lambda args: gbt_phase_scan.create_configuration(args.inputfiles, args.outputdir)
    )

    # "create-config oh" subcommand
    create_config_oh_parser = create_config_subparsers.add_parser("oh")
    create_config_oh_parser.add_argument(
        "-c",
        "--cluster-mask",
        dest="cluster_mask_files",
        type=pathlib.Path,
        action="append",
        help="File containing the delays for the cluster bit masking",
    )
    create_config_oh_parser.add_argument(
        "-s",
        "--sbit-delay",
        dest="sbit_delay_files",
        type=pathlib.Path,
        action="append",
        help="File containing the S-bit delays",
    )
    create_config_oh_parser.add_argument(
        "inputdir",
        type=pathlib.Path,
        help="Input directory from which to read the OH configuration files",
    )
    create_config_oh_parser.add_argument(
        "outputdir",
        type=pathlib.Path,
        nargs="?",
        help="Output directory in which to store the OH configuration files (defaults to <inputdir>)",
    )
    create_config_oh_parser.set_defaults(
        func=lambda args: oh_parameters.create_configuration(
            input_directory=args.inputdir,
            output_directory=args.outputdir,
            cluster_mask_filenames=args.cluster_mask_files,
            sbit_delay_filenames=args.sbit_delay_files,
        )
    )

    # "create-config vfat" subcommand
    create_config_vfat_parser = create_config_subparsers.add_parser("vfat")
    create_config_vfat_parser.add_argument(
        "-m",
        "--mapping",
        dest="mapping_file",
        type=pathlib.Path,
        help="File containing the VFAT chip ID to logical address mapping",
    )
    create_config_vfat_parser.add_argument(
        "-c",
        "--calibration",
        dest="calibration_file",
        type=pathlib.Path,
        help="File containing the VFAT calibration parameters (see gemos query vfat-calibration)",
    )
    create_config_vfat_parser.add_argument(
        "-d",
        "--dac",
        dest="dac_files",
        type=pathlib.Path,
        action="append",
        help="File containing the VFAT DAC scan analysis results",
    )
    create_config_vfat_parser.add_argument(
        "-s",
        "--scurve",
        dest="scurve_files",
        type=pathlib.Path,
        action="append",
        help="File containing the channels mask based on the S-curve analysis",
    )
    create_config_vfat_parser.add_argument(
        "--no-noisy-scurve",
        dest="mask_noisy_scurve",
        default=True,
        action="store_false",
        help="If set, do not mask channels flagged noisy by the S-curve analysis",
    )
    create_config_vfat_parser.add_argument(
        "--no-empty-scurve",
        dest="mask_empty_scurve",
        default=True,
        action="store_false",
        help="If set, do not mask channels flagged empty (i.e. dead) by the S-curve analysis",
    )
    create_config_vfat_parser.add_argument(
        "-t",
        "--threshold",
        dest="threshold_files",
        type=pathlib.Path,
        action="append",
        help="File containing the VFAT thresholds obtained by an S-bit rate scan",
    )
    create_config_vfat_parser.add_argument(
        "-z",
        "--zcc-threshold",
        dest="zcc_threshold_files",
        type=pathlib.Path,
        action="append",
        help="File containing the ZCC VFAT thresholds obtained by an S-bit rate scan",
    )
    create_config_vfat_parser.add_argument(
        "-b",
        "--threshold-bump",
        dest="threshold_bump",
        default=0,
        type=int,
        help="Value, in DAC units, by which to bump the threshold for all VFAT seeing their threshold updated",
    )
    create_config_vfat_parser.add_argument(
        "--hyst-factor",
        type=float,
        dest="hyst_factor",
        default=1.5,
        help="Factor by which to multiply the ARM thresholds to get the hysteresis values when updating the thresholds (negative factors leave the hysteresis unchanged)",
    )
    create_config_vfat_parser.add_argument(
        "-l",
        "--latency",
        dest="latency_files",
        type=pathlib.Path,
        action="append",
        help="File containing the VFAT latencies obtained by a latency scan",
    )
    create_config_vfat_parser.add_argument(
        "--latency-bump",
        dest="latency_bump",
        default=0,
        type=int,
        help="Value, in DAC units, by which to bump the latency for all VFATs seeing their latency updated",
    )
    create_config_vfat_parser.add_argument(
        "-r",
        "--register-set",
        dest="register_set_files",
        type=pathlib.Path,
        action="append",
        help="File containing a set of registers to be applied in the configuration files (see 'gemos dump-config vfat' command)",
    )
    create_config_vfat_parser.add_argument(
        "--default",
        dest="default_file",
        type=pathlib.Path,
        help="File containing default VFAT configuration values",
    )
    create_config_vfat_parser.add_argument(
        "inputdir",
        type=pathlib.Path,
        help="Input directory from which to read the VFAT configuration files",
    )
    create_config_vfat_parser.add_argument(
        "outputdir",
        type=pathlib.Path,
        nargs="?",
        help="Output directory in which to store the VFAT configuration files (defaults to <inputdir>)",
    )
    create_config_vfat_parser.set_defaults(
        func=lambda args: vfat_parameters.create_configuration(
            input_directory=args.inputdir,
            output_directory=args.outputdir,
            default_filename=args.default_file,
            mapping_filename=args.mapping_file,
            calibration_filename=args.calibration_file,
            dac_filenames=args.dac_files,
            threshold_filenames=args.threshold_files,
            zcc_threshold_filenames=args.zcc_threshold_files,
            threshold_bump=args.threshold_bump,
            hyst_factor=args.hyst_factor if args.hyst_factor >= 0 else None,
            latency_filenames=args.latency_files,
            latency_bump=args.latency_bump,
            register_set_filenames=args.register_set_files,
            scurve_filenames=args.scurve_files,
            mask_noisy_scurve=args.mask_noisy_scurve,
            mask_empty_scurve=args.mask_empty_scurve,
        )
    )

    # "dump-config" command
    dump_config_parser = subparsers.add_parser("dump-config")
    dump_config_subparsers = dump_config_parser.add_subparsers(required=True, dest="subcommand")

    # "dump-config vfat" subcommand
    dump_config_vfat_parser = dump_config_subparsers.add_parser("vfat")
    dump_config_vfat_parser.add_argument(
        "inputdir",
        type=pathlib.Path,
        help="Input directory from which to read the VFAT configuration files",
    )
    dump_config_vfat_parser.add_argument(
        "outputfile",
        type=pathlib.Path,
        nargs="?",
        help="Output directory in which to store the VFAT configuration files (defaults to <inputdir>)",
    )
    dump_config_vfat_parser.set_defaults(
        func=lambda args: vfat_parameters.dump_configuration(
            input_directory=args.inputdir,
            output_filename=args.outputfile,
        )
    )

    # "query" command
    query_parser = subparsers.add_parser("query")
    query_subparsers = query_parser.add_subparsers(required=True, dest="subcommand")

    # "query vfat-calibration" subcommand
    query_vfat_calibration = query_subparsers.add_parser("vfat-calibration")
    query_vfat_calibration.add_argument(
        "outputfile",
        type=pathlib.Path,
        help="Output file in which to store the VFAT calibration parameters for all known chips",
    )
    query_vfat_calibration.set_defaults(
        func=lambda args: vfat_calibrations.dump_database(args.outputfile)
    )

    # Parse the command line
    args = parser.parse_args()
    args.func(args)
