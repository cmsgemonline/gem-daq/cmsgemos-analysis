Name:    cmsgemos-analysis
Epoch:   0
Version: %{?_version}%{!?_version:0}
Release: 1%{?dist}
Summary: GEM Online Analysis Suite
License: BSD3
URL:     https://gitlab.cern.ch/cmsgemonline/gem-daq/%{name}

# We are trying to achieve a self-contained package
AutoReqProv: no
Requires: python3

# Using a venv for the installation
%global _venv_root %{buildroot}/opt/%{name}/venv/
%global _venv_python3 %{_venv_root}/bin/python3

# Do not modify the virtual environment installation
%global __os_install_post %{nil}

%global debug_package %{nil}

%description
Analysis Suite for the CMS GEM detectors.

%prep

%build

%install
# Create the virtual environment
mkdir -p %{_venv_root}
%python3 -m venv %{_venv_root}
%{_venv_python3} -m pip install --upgrade pip

# Install our project
%{_venv_python3} -m pip install --no-warn-script-location -f ${RPM_SOURCE_DIR} cmsgemos-analysis==%{version} -r ${RPM_SOURCE_DIR}/requirements-%{version}.txt

# Pre-compile the Python files
%py_byte_compile %{_venv_python3} %{buildroot}

# Relocate all files
find %{_venv_root} -type f -print0 | xargs -0 sed -i 's~%{buildroot}~~'

# Export the cmsgemos-analysis tools in a dedicated path
mkdir -p %{buildroot}/opt/%{name}/bin/
ln -snv ../venv/bin/gemos %{buildroot}/opt/%{name}/bin/

%files
/opt/%{name}/

%changelog
